#include "argumentos.hpp"

namespace bp
{

using namespace std;

string use = "Juego el 'El Barrio, es lo primero";

Argumentos::Argumentos(int32_t e_argc, char* e_argv[]) 
{

    size_t pos = 0;
    while ((pos = use.find("%s")) != string::npos)
        use.replace(pos, 2, string(e_argv[0]));

    if (e_argc == 1)
    {
        cerr <<  "\n[ERROR] [args::args]\n\n" << use << endl;
	return;
    }

    argc = e_argc;
    for (int32_t i = 0; i < argc-1;)
    {
        if (i == 0)
        {
            argv["prog"] = string(e_argv[i]);
            i++;
        } else
        {
            argv[string(e_argv[i])] = string(e_argv[i+1]);
            i += 2;
        }
    }    
}

}


