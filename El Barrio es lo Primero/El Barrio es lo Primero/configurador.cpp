#include "configurador.hpp"

namespace bp
{

using namespace std;

const char* configurador::FASE_INTRO	= "FASE_INTRO";
const char* configurador::FASE_CONFIG	= "FASE_CONFIG";
const char* configurador::CARG_FASES	= "CARG_FASES";
const char* configurador::FASE_FIN		= "FASE_FIN";
const char* configurador::FASE_DEBUG	= "FASE_DEBUG";


configurador::configurador() :

        m_escenario(0),
        m_ico_titulo(0),
        m_frecuencia_audio(0),
        m_formato_audio(0),
        m_canal_audio(0),
        m_caudal_audio(0),
        m_pantalla_ancho(0),
        m_pantalla_alto(0),
        m_pantalla_profundidad(0),
        m_imagenes_por_segundo(0),			
        m_prota(0),
        m_enemigo(0),
        m_s_menu_oculto(make_pair("ROOT0", 0)),	
        m_activado("Activado"), 
        m_desactivado("Desactivado")

{
	InicializarMenuOculto();	
}

configurador::~configurador()
{
	if (m_prota)
		delete m_prota;
	if (m_enemigo)
		delete m_enemigo;	
}

void configurador::InicializarMenuOculto()
{
	
	m_s_menu_oculto.NuevoMenu(make_pair("ROOT0", 0), make_pair("ROOT0", 0), "Configuracion de Pantalla", "", true, true);
	m_s_menu_oculto.NuevoMenu(make_pair("ROOT1", 0), make_pair("ROOT0", 0), "Pantalla completa", m_desactivado, true, false);
	m_s_menu_oculto.NuevoMenu(make_pair("ROOT1", 1), make_pair("ROOT0", 0), "Redimensaionable", m_desactivado, true, false);

	m_s_menu_oculto.NuevoMenu(make_pair("ROOT0", 1), make_pair("ROOT0", 1), "Configuracion de Sonido", "", true, true);
	m_s_menu_oculto.NuevoMenu(make_pair("ROOT2", 0), make_pair("ROOT0", 1), "Sonido encendido", m_activado, true, false);

	m_s_menu_oculto.NuevoMenu(make_pair("ROOT0", 2), make_pair("ROOT0", 2), "Configuracion de Juego", "", true, true);
	m_s_menu_oculto.NuevoMenu(make_pair("ROOT3", 0), make_pair("ROOT0", 2), "Inmortalidad", m_desactivado, true, false);

}

} // namespace bp
