#include "fx.hpp"

namespace bp
{

FX::FX ( void ) :

        m_fx(0)

{

}

FX::~FX ( void )
{
    if (m_fx)
        Mix_FreeChunk(m_fx);
}

bool FX::Cargar ( const char* RutaFx )
{
    m_fx = Mix_LoadWAV(RutaFx);
    if (m_fx == 0)
    {
        SDL_SetError(Mix_GetError());
        return false;
    }
    Mix_AllocateChannels(2);
    
    return true;
}

void FX::Sonar()
{    
    Mix_PlayChannel(1, m_fx, 0);
}

void FX::Parar()
{
  Mix_Pause(-1);
}

} // namespace bp
