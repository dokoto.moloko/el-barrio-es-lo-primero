#ifndef FASE_INTRO_HPP
#define FASE_INTRO_HPP

/*--------------------------------------------------------------------------------------------------------------
* INCLUDES
*-------------------------------------------------------------------------------------------------------------*/
#include "utils.hpp"
#include "fase.hpp"
#include "Imagen.hpp"
#include "Eventos.hpp"
#include "configurador.hpp"
#include "Textos.hpp"
#include "SDL_FrameRate.hpp"
#include "Musica.hpp"

namespace bp
{

/*--------------------------------------------------------------------------------------------------------------
 * CLASS : ACTOR (Eventos, Fase)
 *-------------------------------------------------------------------------------------------------------------*/ 
class Fase_Intro : public Eventos, public fase
{
	/*--------------------------------------------------------------------------------------------------------------
	* PRIVATE ATTRIBUTES
	*--------------------------------------------------------------------------------------------------------------*/
	private:		
		FPSmanager												m_manejadorImgPorSeg;
		bool													m_ejecutandose;
		SDL_Event												m_evento;
		Texto													m_ttf_titulo;
		const char*												m_ruta_ttf;		
		Mix_Music*												m_musica_intro;
		VStr_t													m_historia;	
		SDL_Surface*											m_capa_historia;		
		
	/*--------------------------------------------------------------------------------------------------------------
	* PRIVATE METHODS
	*--------------------------------------------------------------------------------------------------------------*/
	private:
		bool						Inicializar					();
		bool						Iteracion					();
		bool						Dibujar						();
		void						Limpiar						();
		void 						Salir						();		
		void						OnEvent						(SDL_Event* Evento);
		void						OnKeyDown					(SDLKey sym, SDLMod , Uint16 );
		bool						CreaIntro					();
		bool						DibujaIntro					();
		bool						CargarHistoria				();
		bool						GeneraTTF					(SDL_Surface* ImagenDestino, const char* parrafo, int32_t x, int32_t y, int32_t alto);    
		
	/*--------------------------------------------------------------------------------------------------------------
	* PUBLIC METHODS
	*--------------------------------------------------------------------------------------------------------------*/	
	public:
									Fase_Intro					(const char* id, configurador* config);
									~Fase_Intro					(void);
		bool						Ejecutar					();
		
};

} // namespace bp

#endif //FASE_INTRO_HPP
