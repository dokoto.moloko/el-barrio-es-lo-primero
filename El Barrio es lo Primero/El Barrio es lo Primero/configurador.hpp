#ifndef CONFIGURADOR_HPP
#define CONFIGURADOR_HPP

/*--------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------*/
#include "utils.hpp"
#include "Imagen.hpp"
#include "Menu.hpp"
#include "actor.hpp"
#include "ElFuler.hpp"

namespace bp
{

/*--------------------------------------------------------------------------------------------------------------
 * PUBLIC TYPES
 *-------------------------------------------------------------------------------------------------------------*/  
typedef struct											ty_sd_menu
{
  std::string								texto_opcion;
  bool										directorio;
  bool										opcion_activada;
  bool										menu_activado;
  std::vector<ty_s_menu>*					menu_siguiente;
  std::vector<ty_s_menu>*					menu_anterior;

}		
												ty_sd_menu;
/*--------------------------------------------------------------------------------------------------------------
 * CLASS : CONFIGURADOR
 *-------------------------------------------------------------------------------------------------------------*/
class configurador
{
	/*--------------------------------------------------------------------------------------------------------------
	* PRIVATE ATTRIBUTES
	*--------------------------------------------------------------------------------------------------------------*/
	private:
		SDL_Surface*											m_escenario;
		SDL_Surface*											m_ico_titulo;
		int32_t 												m_frecuencia_audio;
		Uint16 													m_formato_audio;
		int32_t 												m_canal_audio;
		int32_t 												m_caudal_audio;
		std::string												m_titulo_juego;
		std::string												m_ruta_icono_juego;
		int32_t													m_pantalla_ancho;
		int32_t													m_pantalla_alto;
		int32_t													m_pantalla_profundidad;
		uint32_t												m_imagenes_por_segundo;
		actor*													m_prota;
		actor*													m_enemigo;	
	
	/*--------------------------------------------------------------------------------------------------------------
	* PRIVATE ENUMS
	*--------------------------------------------------------------------------------------------------------------*/
	private:
		enum													{ancho_menu_oculto = 5};	

	/*--------------------------------------------------------------------------------------------------------------
	* PRIVATE METHODS
	*--------------------------------------------------------------------------------------------------------------*/
	private:
		std::string					DefineRutaRaiz				();
		void						CargaRutas					();
		void						InicializarMenuOculto		();

	/*--------------------------------------------------------------------------------------------------------------
	* PUBLIC ATTRIBUTES
	*--------------------------------------------------------------------------------------------------------------*/
	public:
    // ATRIBUTOS
		static const char*											FASE_INTRO;
		static const char*											FASE_CONFIG;
		static const char*											CARG_FASES;
		static const char*											FASE_FIN;
		static const char*											FASE_DEBUG;
		Menu														m_s_menu_oculto;
		const std::string											m_activado, m_desactivado;

	/*--------------------------------------------------------------------------------------------------------------
	* PUBLIC ATTRIBUTES
	*--------------------------------------------------------------------------------------------------------------*/
	public:    
									configurador				();
		virtual 					~configurador				();
            
		SDL_Surface*&				Escenario					() { return m_escenario; }
		SDL_Surface*&				IconoTitulo					() { return m_ico_titulo; }
		int32_t&					Frecuencia_audio			() { return m_frecuencia_audio; }
		Uint16&						Formato_audio				() { return m_formato_audio; }
		int32_t&					Canal_audio					() { return m_canal_audio; }
		int32_t&					Caudal_audio				() { return m_caudal_audio; }
		std::string&	 			Titulo_juego				() { return m_titulo_juego; }
		std::string&				Ruta_icono_juego			() { return m_ruta_icono_juego; }
		int32_t&					Pantalla_Ancho				() { return m_pantalla_ancho; }
		int32_t&					Pantalla_Alto				() { return m_pantalla_alto; }
		int32_t&					Pantalla_Profundidad		() { return m_pantalla_profundidad; }
		uint32_t&					Imagenes_por_Segundo		() { return m_imagenes_por_segundo; }
		int32_t						ObtenerLongMenu				() { return ancho_menu_oculto; }
		actor*&						ObtenerActor				() { return m_prota; }
		actor*&						ObtenerEnemigo				() { return m_enemigo; }	

};

}
#endif // CONFIGURADOR_HPP
