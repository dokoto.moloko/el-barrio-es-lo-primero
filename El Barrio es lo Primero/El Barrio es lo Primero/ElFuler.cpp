#include "ElFuler.hpp"

#define INIT_GUIA_0(num_mov) 				\
	for(int32_t i = 0; i< num_mov; i++)		\
  { 										\
  guia.x_accion = 0; guia.y_accion = 0; 	\
  mov.guia.push_back(guia); 				\
  }

namespace bp
{

	using namespace std;

	ElFuler::ElFuler
        (
            SDL_Rect PosInicial,
            int32_t Perfil,
            Color_t colorTransp,
            const char* ruta_perfil_der,
            const char* ruta_perfil_izq,
            const int32_t vida
        ) :

		actor("El Fuler", Perfil, PosInicial, vida),
		m_desp_x(mov_stat()->pos_actual.x), 
		m_desp_y(mov_stat()->pos_actual.x),
        m_enemy(NULL)

	{
		SetDelay(0);
		CargarImagenes(ruta_perfil_der, ruta_perfil_izq);	
		Imagen::CrearTransparencia(imagen(perfilDer), colorTransp.R, colorTransp.G, colorTransp.B);
		Imagen::CrearTransparencia(imagen(perfilIzq), colorTransp.R, colorTransp.G, colorTransp.B);

		CargaMovDescanso();
		CargaMovAvanza();
		CargaMovRetrocede();
		CargaPatada();
		CargaGolpeMano();
		CargaAgacharse();
		CargaSalto();
		CargaPatadaCircular();
		CargaPatadaBaja();
		CargarGolpeManoBajo();
		CargarGolpeManoAlto();
		CargaBolaFuego();	
		CargarSaltoLargo();
		CargarPatadaHelocop();
        cargarRecepGolpeN1();
        cargarRecepGolpeN2();
        cargarRecepGolpeN3();
	}



	/* MOVIMIENTOS
	* ---------------------------------------------------------------------------------------------------------------------------------------
	* 200 pixeles el maximo de altura permitida por lo que todas las imagenes deben estar alineadas a la 
	* base de este limite en 200 px. Sumandose 200px por cada secuencia de sprites.
	* Como Ejemplo
	* Descanso.y  = 0
	* Avanza.Y    = 200
	* Retroceso.Y = 400
	* .... ULTIMO.MOVIMIENTO.Y * 200.	
	* ---------------------------------------------------------------------------------------------------------------------------------------
	*/

	void ElFuler::CargaMovDescanso()
	{
		// Movimiento : DESCANSO
		ty_s_guia_mov guia;
		ty_s_conf_mov mov;
		// POSICION DEL SPRITE
		enum { sprite_y = 0 };

		// Recorte sobre la imagen general
		SDL_Rect pos = {0, 0, 0, 0};
		pos.x = 0; pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);	

		//pos.x = static_cast<Sint16>( pos.x + static_cast<Sint16>(pos.w)); 	
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;		
		mov.sec_img.push_back(pos);	
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);

		// Deplazamiento por movimiento
		INIT_GUIA_0(3);

		// Atributos
		mov.imagen = imagen(perfilDer);
		mov.numero = mov.sec_img.size();
		mov.fuerza = 0;
		mov.retardo = 10;

		// Generamos el nuevo movimiento : Perfil Derecho
		CargarMovimiento(make_pair(descanso, perfilDer), mov);

		// Generamos el nuevo movimiento : Perfil Izquierdo
		mov.imagen = imagen(perfilIzq);
		CargarMovimiento(make_pair(descanso, perfilIzq), mov);
	}


	void ElFuler::CargaMovAvanza()
	{
		// Movimiento : AVANZA
		ty_s_guia_mov guia_perf_der;
		ty_s_conf_mov mov;
		// POSICION DEL SPRITE
		enum { sprite_y = 200 };

		// Recorte sobre la imagen general
		SDL_Rect pos = {0, 0, 0, 0};
		pos.x = 0; pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);


		// Deplazamiento por movimiento : Perfil Derecho
		guia_perf_der.x_accion = 10; guia_perf_der.y_accion = 0;
		mov.guia.push_back(guia_perf_der);
		guia_perf_der.x_accion = 10; guia_perf_der.y_accion = 0;
		mov.guia.push_back(guia_perf_der);
		guia_perf_der.x_accion = 10; guia_perf_der.y_accion = 0;
		mov.guia.push_back(guia_perf_der);
		guia_perf_der.x_accion = 10; guia_perf_der.y_accion = 0;
		mov.guia.push_back(guia_perf_der);


		// Atributos
		mov.imagen = imagen(perfilDer);
		mov.numero = mov.sec_img.size();
		mov.fuerza = 0;
		mov.retardo = 2;	

		// Generamos el nuevo movimiento : Perfil Derecho
		CargarMovimiento(make_pair(avance, perfilDer), mov);

		// Generamos el nuevo movimiento : Perfil Izquierdo
		mov.imagen = imagen(perfilIzq);
		CargarMovimiento(make_pair(avance, perfilIzq), mov);
	}

	void ElFuler::CargaMovRetrocede()
	{
		// Movimiento : RETROCESO
		ty_s_guia_mov guia_perf_der;
		ty_s_conf_mov mov;
		// POSICION DEL SPRITE
		enum { sprite_y = 400 }; 

		// Recorte sobre la imagen general
		SDL_Rect pos = {0, 0, 0, 0};
		pos.x = 0; pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);

		// Deplazamiento por movimiento
		guia_perf_der.x_accion = -10; guia_perf_der.y_accion = 0;
		mov.guia.push_back(guia_perf_der);
		guia_perf_der.x_accion = -10; guia_perf_der.y_accion = 0;
		mov.guia.push_back(guia_perf_der);
		guia_perf_der.x_accion = -10; guia_perf_der.y_accion = 0;
		mov.guia.push_back(guia_perf_der);
		guia_perf_der.x_accion = -10; guia_perf_der.y_accion = 0;
		mov.guia.push_back(guia_perf_der);

		// Atributos
		mov.imagen = imagen(perfilDer);
		mov.numero = mov.sec_img.size();
		mov.fuerza = 0;
		mov.retardo = 2;

		// Generamos el nuevo movimiento : Perfil Derecho
		CargarMovimiento(make_pair(retorno, perfilDer), mov);

		// Generamos el nuevo movimiento : Perfil Izquierdo
		mov.imagen = imagen(perfilIzq);
		CargarMovimiento(make_pair(retorno, perfilIzq), mov);
	}


	void ElFuler::CargaPatada()
	{
		// Movimiento : PATADA
		ty_s_guia_mov guia;
		ty_s_conf_mov mov;
		// POSICION DEL SPRITE
		enum { sprite_y = 600 }; 

		// Recorte sobre la imagen general
		SDL_Rect pos = {0, 0, 0, 0};
		pos.x = 0; pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);	
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.y = sprite_y; pos.w = 150; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);	
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);

		// Deplazamiento por movimiento
		INIT_GUIA_0(3);

		// Atributos
		mov.imagen = imagen(perfilDer);
		mov.numero = mov.sec_img.size();
		mov.fuerza = 2;
		mov.retardo = 5;

		// Generamos el nuevo movimiento: Perfil Derecho
		CargarMovimiento(make_pair(patada, perfilDer), mov);
		// Generamos el nuevo movimiento: Perfil Izquierdo
		mov.imagen = imagen(perfilIzq);
		CargarMovimiento(make_pair(patada, perfilIzq), mov);
	}

	void ElFuler::CargaGolpeMano()
	{
		// Movimiento : GOLPE MANO
		ty_s_guia_mov guia;
		ty_s_conf_mov mov;
		// POSICION DEL SPRITE
		enum { sprite_y = 800 };

		// Recorte sobre la imagen general
		SDL_Rect pos = {0, 0, 0, 0};
		pos.x = 0; pos.y = sprite_y; pos.w = 150; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);

		// Deplazamiento por movimiento
		INIT_GUIA_0(2);

		// Atributos
		mov.imagen = imagen(perfilDer);
		mov.numero = mov.sec_img.size();
		mov.fuerza = 1;
		mov.retardo = 5;	
		//mov.retardo = 35;	

		// Generamos el nuevo movimiento : Perfil Derecho
		CargarMovimiento(make_pair(golpeo_mano, perfilDer), mov);
		// Generamos el nuevo movimiento : Perfil Izquierdo	
		mov.imagen = imagen(perfilIzq);
		CargarMovimiento(make_pair(golpeo_mano, perfilIzq), mov);
	}

	void ElFuler::CargaAgacharse()
	{
		// Movimiento : AGACHARSE
		ty_s_guia_mov guia;
		ty_s_conf_mov mov;
		// POSICION DEL SPRITE
		enum { sprite_y = 1000 };

		// Recorte sobre la imagen general
		SDL_Rect pos = {0, 0, 0, 0};
		pos.x = 0; pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);

		// Deplazamiento por movimiento
		INIT_GUIA_0(1);

		// Atributos
		mov.imagen = imagen(perfilDer);
		mov.numero = mov.sec_img.size();
		mov.fuerza = 0;
		mov.retardo = 10;		

		// Generamos el nuevo movimiento : Perfil Derecho
		CargarMovimiento(make_pair(agacharse, perfilDer), mov);
		// Generamos el nuevo movimiento : Perfil Izquierdo
		mov.imagen = imagen(perfilIzq);
		CargarMovimiento(make_pair(agacharse, perfilIzq), mov);
	}

	void ElFuler::CargaSalto()
	{
		// Movimiento : SALTO
		ty_s_guia_mov guia;
		ty_s_conf_mov mov;
		// POSICION DEL SPRITE
		enum { sprite_y = 1200 };

		// Recorte sobre la imagen general
		SDL_Rect pos = {0, 0, 0, 0};
		pos.x = 0; pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.y = sprite_y-20; pos.w = 100; pos.h = ALTURA_SPRITE+20;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);

		// Deplazamiento por movimiento
		guia.x_accion = 0; guia.y_accion = -50;
		mov.guia.push_back(guia);
		guia.x_accion = 0; guia.y_accion = -50;
		mov.guia.push_back(guia);
		guia.x_accion = 0; guia.y_accion = -50;
		mov.guia.push_back(guia);
		guia.x_accion = 0; guia.y_accion = 150;
		mov.guia.push_back(guia);
		guia.x_accion = 0; guia.y_accion = 0;
		mov.guia.push_back(guia);

		// Atributos
		mov.imagen = imagen(perfilDer);
		mov.numero = mov.sec_img.size();
		mov.fuerza = 0;
		mov.retardo = 6;	

		// Generamos el nuevo movimiento : Perfil Derecho
		actor::CargarMovimiento(make_pair(salto_arriba, perfilDer), mov);
		// Generamos el nuevo movimiento : Perfil Izquierdo
		mov.imagen = imagen(perfilIzq);
		actor::CargarMovimiento(make_pair(salto_arriba, perfilIzq), mov);
	}

	void ElFuler::CargaPatadaCircular()			
	{
		// Movimiento : PATADA CIRCULAR
		ty_s_guia_mov guia;
		ty_s_conf_mov mov;
		// POSICION DEL SPRITE
		enum { sprite_y = 1400 };

		// Recorte sobre la imagen general
		SDL_Rect pos = {0, 0, 0, 0};
		pos.x = 0; pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);		
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.y = sprite_y; pos.w = 130; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);	
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.y = sprite_y; pos.w = 150; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);	
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.y = sprite_y; pos.w = 130; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);	
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);  

		// Deplazamiento por movimiento
		INIT_GUIA_0(5);

		// Atributos
		mov.imagen = imagen(perfilDer);
		mov.numero = mov.sec_img.size();
		mov.fuerza = 10;
		mov.retardo = 4;	

		// Generamos el nuevo movimiento : Perfil Derecho
		actor::CargarMovimiento(make_pair(patada_circular, perfilDer), mov);
		// Generamos el nuevo movimiento : Perfil Izquierdo
		mov.imagen = imagen(perfilIzq);
		actor::CargarMovimiento(make_pair(patada_circular, perfilIzq), mov);	
	}

	void ElFuler::CargaPatadaBaja()
	{
		// Movimiento : PATADA BAJA
		ty_s_guia_mov guia;
		ty_s_conf_mov mov;
		// POSICION DEL SPRITE
		enum { sprite_y = 1600 };

		// Recorte sobre la imagen general
		SDL_Rect pos = {0, 0, 0, 0};
		pos.x = 0; pos.y = sprite_y; pos.w = 180; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);

		// Deplazamiento por movimiento
		INIT_GUIA_0(2);

		// Atributos
		mov.imagen = imagen(perfilDer);
		mov.numero = mov.sec_img.size();
		mov.fuerza = 2;
		mov.retardo = 5;		

		// Generamos el nuevo movimiento : Perfil Derecho
		CargarMovimiento(make_pair(patada_baja, perfilDer), mov);
		// Generamos el nuevo movimiento : Perfil Izquierdo	
		mov.imagen = imagen(perfilIzq);
		CargarMovimiento(make_pair(patada_baja, perfilIzq), mov);
	}

	void ElFuler::CargarGolpeManoBajo()
	{
		// Movimiento : GOLPE MANO BAJO
		ty_s_guia_mov guia;
		ty_s_conf_mov mov;
		// POSICION DEL SPRITE
		enum { sprite_y = 1800 };

		// Recorte sobre la imagen general
		SDL_Rect pos = {0, 0, 0, 0};
		pos.x = 0; pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 150; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);

		// Deplazamiento por movimiento
		INIT_GUIA_0(2);

		// Atributos
		mov.imagen = imagen(perfilDer);
		mov.numero = mov.sec_img.size();
		mov.fuerza = 2;
		mov.retardo = 5;		

		// Generamos el nuevo movimiento : Perfil Derecho
		CargarMovimiento(make_pair(golpeo_mano_bajo, perfilDer), mov);
		// Generamos el nuevo movimiento : Perfil Izquierdo	
		mov.imagen = imagen(perfilIzq);
		CargarMovimiento(make_pair(golpeo_mano_bajo, perfilIzq), mov);  
	}

	void ElFuler::CargarGolpeManoAlto()
	{
		// Movimiento : GOLPE MANO ALTO
		ty_s_guia_mov guia;
		ty_s_conf_mov mov;
		// POSICION DEL SPRITE
		enum { sprite_y = 2000 };

		// Recorte sobre la imagen general
		SDL_Rect pos = {0, 0, 0, 0};
		pos.x = 0; pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 150; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);	

		// Deplazamiento por movimiento
		guia.x_accion = 0; guia.y_accion = -50;
		mov.guia.push_back(guia);
		guia.x_accion = 0; guia.y_accion = -50;
		mov.guia.push_back(guia);
		guia.x_accion = 0; guia.y_accion = -50;
		mov.guia.push_back(guia);
		guia.x_accion = 0; guia.y_accion = 100;
		mov.guia.push_back(guia);
		guia.x_accion = 0; guia.y_accion = 50;
		mov.guia.push_back(guia);
		guia.x_accion = 0; guia.y_accion = 0;
		mov.guia.push_back(guia);	

		// Atributos
		mov.imagen = imagen(perfilDer);
		mov.numero = mov.sec_img.size();
		mov.fuerza = 2;
		mov.retardo = 7;		

		// Generamos el nuevo movimiento : Perfil Derecho
		CargarMovimiento(make_pair(golpe_mano_alto, perfilDer), mov);
		// Generamos el nuevo movimiento : Perfil Izquierdo	
		mov.imagen = imagen(perfilIzq);
		CargarMovimiento(make_pair(golpe_mano_alto, perfilIzq), mov); 
	}

	void ElFuler::CargaBolaFuego()
	{
		// Movimiento : BOLA DE FUEGO
		ty_s_guia_mov guia;
		ty_s_conf_mov mov;
		// POSICION DEL SPRITE
		enum { sprite_y = 2200 };

		// Recorte sobre la imagen general
		SDL_Rect pos = {0, 0, 0, 0};
		pos.x = 0; pos.y = sprite_y; pos.w = 120; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 125; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 160; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 50; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 20; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 50; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);	

		// Deplazamiento por movimiento
		INIT_GUIA_0(7);		

		// Atributos
		mov.imagen = imagen(perfilDer);
		mov.numero = 3;
		mov.fuerza = 20;
		mov.retardo = 7;				

		// Generamos el nuevo movimiento : Perfil Derecho
		CargarMovimiento(make_pair(bola_fuego, perfilDer), mov);
		// Generamos el nuevo movimiento : Perfil Izquierdo	
		mov.imagen = imagen(perfilIzq);
		CargarMovimiento(make_pair(bola_fuego, perfilIzq), mov); 
	}

	void ElFuler::CargarSaltoLargo()
	{
		// Movimiento : SALTO LARGO
		ty_s_guia_mov guia;
		ty_s_conf_mov mov;
		// POSICION DEL SPRITE
		enum { sprite_y = 2400 };

		// Recorte sobre la imagen general
		SDL_Rect pos = {0, 0, 0, 0};
		pos.x = 0; pos.y = sprite_y; pos.w = 90; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 150; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 70; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 150; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 120; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);		

		// Deplazamiento por movimiento
		guia.x_accion = 40; guia.y_accion = -50;
		mov.guia.push_back(guia);
		guia.x_accion = 40; guia.y_accion = -10;
		mov.guia.push_back(guia);
		guia.x_accion = 40; guia.y_accion = -10;
		mov.guia.push_back(guia);
		guia.x_accion = 40; guia.y_accion = 10;
		mov.guia.push_back(guia);
		guia.x_accion = 40; guia.y_accion = 60;
		mov.guia.push_back(guia);			

		// Atributos
		mov.imagen = imagen(perfilDer);
		mov.numero = mov.sec_img.size();
		mov.fuerza = 15;
		mov.retardo = 8;				

		// Generamos el nuevo movimiento : Perfil Derecho
		CargarMovimiento(make_pair(salto_largo, perfilDer), mov);
		// Generamos el nuevo movimiento : Perfil Izquierdo	
		mov.imagen = imagen(perfilIzq);
		CargarMovimiento(make_pair(salto_largo, perfilIzq), mov);

	}

	void ElFuler::CargarPatadaHelocop()
	{
		// Movimiento : PATADA HELICOPTERO
		ty_s_guia_mov guia;
		ty_s_conf_mov mov;
		// POSICION DEL SPRITE
		enum { sprite_y = 2600 };

		// Recorte sobre la imagen general
		SDL_Rect pos = {0, 0, 0, 0};
		pos.x = 0; pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 120; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 80; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 90; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 110; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 90; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);

		// Deplazamiento por movimiento
		guia.x_accion = 40; guia.y_accion = -50;
		mov.guia.push_back(guia);
		guia.x_accion = 40; guia.y_accion = 0;
		mov.guia.push_back(guia);
		guia.x_accion = 60; guia.y_accion = 0;
		mov.guia.push_back(guia);
		guia.x_accion = 60; guia.y_accion = 0;
		mov.guia.push_back(guia);
		guia.x_accion = 40; guia.y_accion = 0;
		mov.guia.push_back(guia);
		guia.x_accion = 40; guia.y_accion = 50;
		mov.guia.push_back(guia);		

		// Atributos
		mov.imagen = imagen(perfilDer);
		mov.numero = mov.sec_img.size();
		mov.fuerza = 15;
		mov.retardo = 14;				

		// Generamos el nuevo movimiento : Perfil Derecho
		CargarMovimiento(make_pair(patada_helicop, perfilDer), mov);
		// Generamos el nuevo movimiento : Perfil Izquierdo	
		mov.imagen = imagen(perfilIzq);
		CargarMovimiento(make_pair(patada_helicop, perfilIzq), mov); 
	}

    /*
     Aqui spritre_y deberia ser 2800, pero como considero que la recepcion de golpe
     que enpieza en 3000 es mas suave y se equipara mas con la idea de NIVEL 1 empezamos
     en 3000 = NIVEL 1 , 2800 = NIVEL 2, 3200 = NIVEL 3
     */    
    void ElFuler::cargarRecepGolpeN1()
    {
        // Movimiento : RECEPCION DE GOLPE NIVEL 1
		ty_s_guia_mov guia;
		ty_s_conf_mov mov;
		// POSICION DEL SPRITE
		enum { sprite_y = 3000 };
        
        
        // Recorte sobre la imagen general
		SDL_Rect pos = {0, 0, 0, 0};
		pos.x = 0; pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
        
        guia.x_accion = 40; guia.y_accion = 0;
		mov.guia.push_back(guia);
		guia.x_accion = 40; guia.y_accion = 0;
		mov.guia.push_back(guia);
        
        
        // Atributos
		mov.imagen = imagen(perfilDer);
		mov.numero = mov.sec_img.size();
		mov.fuerza = 0;
		mov.retardo = 10;
        
		// Generamos el nuevo movimiento : Perfil Derecho
		CargarMovimiento(make_pair(recep_golpe_N1, perfilDer), mov);
		// Generamos el nuevo movimiento : Perfil Izquierdo
		mov.imagen = imagen(perfilIzq);
		CargarMovimiento(make_pair(recep_golpe_N1, perfilIzq), mov);
        
    }

    void ElFuler::cargarRecepGolpeN2()
    {
        // Movimiento : RECEPCION DE GOLPE NIVEL 2
		ty_s_guia_mov guia;
		ty_s_conf_mov mov;
		// POSICION DEL SPRITE
		enum { sprite_y = 2800 };
        
        // Recorte sobre la imagen general
		SDL_Rect pos = {0, 0, 0, 0};
		pos.x = 0; pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
        
        guia.x_accion = 40; guia.y_accion = 0;
		mov.guia.push_back(guia);
		guia.x_accion = 40; guia.y_accion = 0;
		mov.guia.push_back(guia);
		guia.x_accion = 40; guia.y_accion = 0;
		mov.guia.push_back(guia);
        
        
        // Atributos
		mov.imagen = imagen(perfilDer);
		mov.numero = mov.sec_img.size();
		mov.fuerza = 0;
		mov.retardo = 10;
        
		// Generamos el nuevo movimiento : Perfil Derecho
		CargarMovimiento(make_pair(recep_golpe_N2, perfilDer), mov);
		// Generamos el nuevo movimiento : Perfil Izquierdo
		mov.imagen = imagen(perfilIzq);
		CargarMovimiento(make_pair(recep_golpe_N2, perfilIzq), mov);

    }
    
    void ElFuler::cargarRecepGolpeN3()
    {
        // Movimiento : RECEPCION DE GOLPE NIVEL 3
		ty_s_guia_mov guia;
		ty_s_conf_mov mov;
		// POSICION DEL SPRITE
		enum { sprite_y = 3200 };
        
        // Recorte sobre la imagen general
		SDL_Rect pos = {0, 0, 0, 0};
		pos.x = 0; pos.y = sprite_y; pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 100; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
		pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 80; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
        pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 150; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
        pos.x = conv::sSint16toUint16(pos.x, pos.w); pos.w = 150; pos.h = ALTURA_SPRITE;
		mov.sec_img.push_back(pos);
        
        guia.x_accion = 40; guia.y_accion = 0;
		mov.guia.push_back(guia);
		guia.x_accion = 40; guia.y_accion = 0;
		mov.guia.push_back(guia);
		guia.x_accion = 40; guia.y_accion = 0;
		mov.guia.push_back(guia);
        guia.x_accion = 40; guia.y_accion = 0;
		mov.guia.push_back(guia);
        guia.x_accion = 40; guia.y_accion = 0;
		mov.guia.push_back(guia);
        
        
        // Atributos
		mov.imagen = imagen(perfilDer);
		mov.numero = mov.sec_img.size();
		mov.fuerza = 0;
		mov.retardo = 10;
        
		// Generamos el nuevo movimiento : Perfil Derecho
		CargarMovimiento(make_pair(recep_golpe_N3, perfilDer), mov);
		// Generamos el nuevo movimiento : Perfil Izquierdo
		mov.imagen = imagen(perfilIzq);
		CargarMovimiento(make_pair(recep_golpe_N3, perfilIzq), mov);
    }
    
	void ElFuler::GestContMov(CoInt_t TipoMov, CoInt_t Perfil)
	{
		Uint8 *keystate = SDL_GetKeyState(NULL);
		if (mov_stat()->repeticion[TipoMov] == false) 
		{
			if (mov_stat()->repeticion[avance] == true && keystate[conv::KeyToSDLKey(avance)])
			{
				ActivaTipoMovimiento(avance, Perfil);
				mov_stat()->repeticion[avance] = true;	
				SetDelay(0);
			} 
			else if(mov_stat()->repeticion[retorno] == true  && keystate[conv::KeyToSDLKey(retorno)])
			{
				ActivaTipoMovimiento(retorno, Perfil);
				mov_stat()->repeticion[retorno] = true;	
				SetDelay(0);
			}
			else if (mov_stat()->repeticion[agacharse] == true && keystate[conv::KeyToSDLKey(agacharse)])
			{
				ActivaTipoMovimiento(agacharse, Perfil);
				mov_stat()->repeticion[agacharse] = true;	
				SetDelay(0);
			}			
			else
				DesActivaTipoMovimiento(); 		  
		}
		else mov_stat()->ulti_sec = 0;	
	}

	void ElFuler::MovSinDespPerp(SDL_Surface* ImagenDestino, int32_t TipoMov, int32_t Perfil)
	{
		ty_s_conf_mov* mov = mov_conf(make_pair(TipoMov, Perfil));
		static int32_t delay = 0;	

		Imagen::Dibujar(ImagenDestino, mov->imagen, mov_stat()->pos_actual.x, mov_stat()->pos_actual.y, mov->sec_img.at(mov_stat()->ulti_sec).x, mov->sec_img.at(mov_stat()->ulti_sec).y, mov->sec_img.at(mov_stat()->ulti_sec).w, mov->sec_img.at(mov_stat()->ulti_sec).h);	
		if (delay++ == mov->retardo)
		{
			mov_stat()->ulti_sec = (mov_stat()->ulti_sec == mov->numero-1)? 0 : mov_stat()->ulti_sec + 1;
			delay = 0;
		}
	}

	void ElFuler::MovSinDesp(SDL_Surface* ImagenDestino, int32_t TipoMov, int32_t Perfil, int32_t LastMov)
	{
		ty_s_conf_mov* mov = mov_conf(make_pair(TipoMov, Perfil));
		ty_s_conf_mov* Lastmov = mov_conf(make_pair(LastMov, Perfil));		

		if (mov_stat()->ulti_sec < mov->numero)
		{	  	
			int32_t x = mov_stat()->pos_actual.x;
			if (Perfil == perfilIzq && mov->sec_img.at(mov_stat()->ulti_sec).w > Lastmov->sec_img.at(0).w)
				x = (mov_stat()->pos_actual.x + Lastmov->sec_img.at(0).w) - mov->sec_img.at(mov_stat()->ulti_sec).w;

			Imagen::Dibujar(ImagenDestino, mov->imagen, x, mov_stat()->pos_actual.y, mov->sec_img.at(mov_stat()->ulti_sec).x, mov->sec_img.at(mov_stat()->ulti_sec).y, mov->sec_img.at(mov_stat()->ulti_sec).w, mov->sec_img.at(mov_stat()->ulti_sec).h);			
			if (m_delay++ == static_cast<int32_t>(mov->retardo))
			{
				SimulaMovimiento(mov_stat()->pos_actual.x, mov_stat()->pos_actual.y, ImagenDestino->w, ImagenDestino->h);
				ConsolidaMovimiento();
				mov_stat()->ulti_sec++;
				SetDelay(0);
			}
		} 
		else
		{
			Imagen::Dibujar(ImagenDestino, Lastmov->imagen, mov_stat()->pos_actual.x, mov_stat()->pos_actual.y, Lastmov->sec_img.at(0).x, Lastmov->sec_img.at(0).y, Lastmov->sec_img.at(0).w, Lastmov->sec_img.at(0).h);			
			GestContMov(TipoMov, Perfil);
		}
	}

	void ElFuler::MovDisparo(SDL_Surface* ImagenDestino, int32_t TipoMov, int32_t Perfil, int32_t LastMov)
	{		
		ty_s_conf_mov* mov = mov_conf(make_pair(TipoMov, Perfil));
		ty_s_conf_mov* Lastmov = mov_conf(make_pair(LastMov, Perfil));		

		if (mov_stat()->ulti_sec < mov->numero)
		{	  	
			int32_t x = mov_stat()->pos_actual.x;
			if (Perfil == perfilIzq && mov->sec_img.at(mov_stat()->ulti_sec).w > Lastmov->sec_img.at(0).w)
				x = (mov_stat()->pos_actual.x + Lastmov->sec_img.at(0).w) - mov->sec_img.at(mov_stat()->ulti_sec).w;

			Imagen::Dibujar(ImagenDestino, mov->imagen, x, mov_stat()->pos_actual.y, mov->sec_img.at(mov_stat()->ulti_sec).x, mov->sec_img.at(mov_stat()->ulti_sec).y, mov->sec_img.at(mov_stat()->ulti_sec).w, mov->sec_img.at(mov_stat()->ulti_sec).h);			
			if (m_delay++ == static_cast<int32_t>(mov->retardo))
			{
				SimulaMovimiento(mov_stat()->pos_actual.x, mov_stat()->pos_actual.y, ImagenDestino->w, ImagenDestino->h);
				ConsolidaMovimiento();
				mov_stat()->ulti_sec++;
				SetDelay(0);
			}
		} 
		else if (mov_stat()->ulti_sec == mov->numero)
		{			
			int32_t x = mov_stat()->pos_actual.x;
			if (Perfil == perfilIzq && mov->sec_img.at(mov->numero-1).w > Lastmov->sec_img.at(0).w)
				x = (mov_stat()->pos_actual.x + Lastmov->sec_img.at(0).w) - mov->sec_img.at(mov->numero-1).w;

			Imagen::Dibujar(ImagenDestino, mov->imagen, x, mov_stat()->pos_actual.y, mov->sec_img.at(mov->numero-1).x, mov->sec_img.at(mov->numero-1).y, mov->sec_img.at(mov->numero-1).w, mov->sec_img.at(mov->numero-1).h);					 
			
			static int32_t x_proyectil = 0;  
			static bool primera_vez = true;
			if (Perfil == perfilDer && primera_vez)
				x_proyectil = mov_stat()->pos_actual.x + mov->sec_img.at(mov->numero).w + mov->sec_img.at(mov->numero+1).w;  
			else if(Perfil == perfilIzq && primera_vez)
				x_proyectil = mov_stat()->pos_actual.x - mov->sec_img.at(mov->numero).w - mov->sec_img.at(mov->numero+1).w;  
			primera_vez = false;

			bool contin = true;
			if (m_delay++ == static_cast<int32_t>(mov->retardo))
			{
				if (Perfil == perfilDer)
				{
					x_proyectil += mov->sec_img.at(mov->numero-1).w;
					contin = (x_proyectil < ImagenDestino->w) ? true : false;
				}
				else if(Perfil == perfilIzq)
				{
					x_proyectil -= mov->sec_img.at(mov->numero-1).w;
					contin = (x_proyectil > 0) ? true : false;
				}		
				SetDelay(0);
			}

			Imagen::Dibujar(ImagenDestino, mov->imagen, x_proyectil, mov_stat()->pos_actual.y, mov->sec_img.at(mov->numero).x, mov->sec_img.at(mov->numero).y, mov->sec_img.at(mov->numero).w, mov->sec_img.at(mov->numero).h);					 			  		
			if (contin == false)
			{
				x_proyectil = mov_stat()->pos_actual.x + mov->sec_img.at(mov->numero).w + mov->sec_img.at(mov->numero+1).w;
				if (Perfil == perfilDer)
					x_proyectil = mov_stat()->pos_actual.x + mov->sec_img.at(mov->numero).w + mov->sec_img.at(mov->numero+1).w;  
				else if(Perfil == perfilIzq)
					x_proyectil = mov_stat()->pos_actual.x - mov->sec_img.at(mov->numero).w - mov->sec_img.at(mov->numero+1).w;  
				mov_stat()->ulti_sec++;		
				primera_vez = true;
			}			
		}
		else
		{
			Imagen::Dibujar(ImagenDestino, Lastmov->imagen, mov_stat()->pos_actual.x, mov_stat()->pos_actual.y, Lastmov->sec_img.at(0).x, Lastmov->sec_img.at(0).y, Lastmov->sec_img.at(0).w, Lastmov->sec_img.at(0).h);						
			GestContMov(TipoMov, Perfil);
		}		
	}


    void ElFuler::MovRecepGolpe(SDL_Surface* ImagenDestino, int32_t TipoMov, int32_t Perfil, int32_t LastMov)
    {
		ty_s_conf_mov* mov = mov_conf(make_pair(TipoMov, Perfil));
		ty_s_conf_mov* Lastmov = mov_conf(make_pair(LastMov, Perfil));
        
		if (mov_stat()->ulti_sec < mov->numero)
		{
			int32_t x = mov_stat()->pos_actual.x;
			if (Perfil == perfilIzq && mov->sec_img.at(mov_stat()->ulti_sec).w > Lastmov->sec_img.at(0).w)
				x = (mov_stat()->pos_actual.x + Lastmov->sec_img.at(0).w) + mov->sec_img.at(mov_stat()->ulti_sec).w;
            
			Imagen::Dibujar(ImagenDestino, mov->imagen, x, mov_stat()->pos_actual.y, mov->sec_img.at(mov_stat()->ulti_sec).x, mov->sec_img.at(mov_stat()->ulti_sec).y, mov->sec_img.at(mov_stat()->ulti_sec).w, mov->sec_img.at(mov_stat()->ulti_sec).h);
            
			if (m_delay++ == static_cast<int32_t>(mov->retardo))
			{
				int32_t x = 0;
                if (Perfil == perfilDer)
				{
					x = mov_stat()->pos_actual.x - mov->guia.at(mov_stat()->ulti_sec).x_accion;
				}
				else if(Perfil == perfilIzq)
				{
					x = mov_stat()->pos_actual.x + mov->guia.at(mov_stat()->ulti_sec).x_accion;
				}
				SimulaMovimiento(x, mov_stat()->pos_actual.y, ImagenDestino->w, ImagenDestino->h);
				ConsolidaMovimiento();
				mov_stat()->ulti_sec++;
				SetDelay(0);
			}
		}
		else
		{
			Imagen::Dibujar(ImagenDestino, Lastmov->imagen, mov_stat()->pos_actual.x, mov_stat()->pos_actual.y, Lastmov->sec_img.at(0).x, Lastmov->sec_img.at(0).y, Lastmov->sec_img.at(0).w, Lastmov->sec_img.at(0).h);
			GestContMov(TipoMov, Perfil);
		}
    }
    
	void ElFuler::MovDespHorizontal(SDL_Surface* ImagenDestino, int32_t TipoMov, int32_t Perfil, int32_t LastMov)
	{
		ty_s_conf_mov* mov = mov_conf(make_pair(TipoMov, Perfil));
		ty_s_conf_mov* Lastmov = mov_conf(make_pair(LastMov, Perfil));

		if (mov_stat()->ulti_sec < mov->numero)
		{		
			int32_t x = mov_stat()->pos_actual.x;
			if (Perfil == perfilIzq && mov->sec_img.at(mov_stat()->ulti_sec).w > Lastmov->sec_img.at(0).w)
				x = (mov_stat()->pos_actual.x + Lastmov->sec_img.at(0).w) - mov->sec_img.at(mov_stat()->ulti_sec).w;
            
			Imagen::Dibujar(ImagenDestino, mov->imagen, x, mov_stat()->pos_actual.y, mov->sec_img.at(mov_stat()->ulti_sec).x, mov->sec_img.at(mov_stat()->ulti_sec).y, mov->sec_img.at(mov_stat()->ulti_sec).w, mov->sec_img.at(mov_stat()->ulti_sec).h);					
			if (m_delay++ == static_cast<int32_t>(mov->retardo))
			{		    
				int32_t x = mov_stat()->pos_actual.x + mov->guia.at(mov_stat()->ulti_sec).x_accion;
				SimulaMovimiento(x, mov_stat()->pos_actual.y, ImagenDestino->w, ImagenDestino->h);
				ConsolidaMovimiento();
				mov_stat()->ulti_sec++;			
				SetDelay(0);
			}	
		}
		else
		{		
			Imagen::Dibujar(ImagenDestino, Lastmov->imagen, mov_stat()->pos_actual.x, mov_stat()->pos_actual.y, Lastmov->sec_img.at(0).x, Lastmov->sec_img.at(0).y, Lastmov->sec_img.at(0).w, Lastmov->sec_img.at(0).h);			
			GestContMov(TipoMov, Perfil);
		}	
	}

	void ElFuler::MovDespHorizontalVertical(SDL_Surface* ImagenDestino, int32_t TipoMov, int32_t Perfil, int32_t LastMov)
	{
		ty_s_conf_mov* mov = mov_conf(make_pair(TipoMov, Perfil));
		ty_s_conf_mov* Lastmov = mov_conf(make_pair(LastMov, Perfil));

		if (mov_stat()->ulti_sec < mov->numero)
		{		
			int32_t x = mov_stat()->pos_actual.x;
			if (Perfil == perfilIzq && mov->sec_img.at(mov_stat()->ulti_sec).w > Lastmov->sec_img.at(0).w)
				x = (mov_stat()->pos_actual.x + Lastmov->sec_img.at(0).w) - mov->sec_img.at(mov_stat()->ulti_sec).w;


			Imagen::Dibujar(ImagenDestino, mov->imagen, x, mov_stat()->pos_actual.y, mov->sec_img.at(mov_stat()->ulti_sec).x, mov->sec_img.at(mov_stat()->ulti_sec).y, mov->sec_img.at(mov_stat()->ulti_sec).w, mov->sec_img.at(mov_stat()->ulti_sec).h);							
			if (m_delay++ == static_cast<int32_t>(mov->retardo))
			{
				int32_t x = 0, y = 0;
				if (Perfil == perfilDer)
				{
					x = mov_stat()->pos_actual.x + mov->guia.at(mov_stat()->ulti_sec).x_accion;
					y = mov_stat()->pos_actual.y + mov->guia.at(mov_stat()->ulti_sec).y_accion;							
				}
				else if(Perfil == perfilIzq)
				{
					x = mov_stat()->pos_actual.x - mov->guia.at(mov_stat()->ulti_sec).x_accion;
					y = mov_stat()->pos_actual.y + mov->guia.at(mov_stat()->ulti_sec).y_accion;							
				}
				SimulaMovimiento(x, y, ImagenDestino->w, ImagenDestino->h);
				ConsolidaMovimiento();
				mov_stat()->ulti_sec++;	
				SetDelay(0);
			}
		} 
		else
		{		
			Imagen::Dibujar(ImagenDestino, Lastmov->imagen, mov_stat()->pos_actual.x, mov_stat()->pos_actual.y, Lastmov->sec_img.at(0).x, Lastmov->sec_img.at(0).y, Lastmov->sec_img.at(0).w, Lastmov->sec_img.at(0).h);			
			GestContMov(TipoMov, Perfil);
		}
	}

	void ElFuler::MovDespVertical(SDL_Surface* ImagenDestino, int32_t TipoMov, int32_t Perfil, int32_t LastMov)
	{
		ty_s_conf_mov* mov = mov_conf(make_pair(TipoMov, Perfil));
		ty_s_conf_mov* Lastmov = mov_conf(make_pair(LastMov, Perfil));

		if (mov_stat()->ulti_sec < mov->numero)
		{
			int32_t x = mov_stat()->pos_actual.x;
			if (Perfil == perfilIzq && mov->sec_img.at(mov_stat()->ulti_sec).w > Lastmov->sec_img.at(0).w)
				x = (mov_stat()->pos_actual.x + Lastmov->sec_img.at(0).w) - mov->sec_img.at(mov_stat()->ulti_sec).w;

			Imagen::Dibujar(ImagenDestino, mov->imagen, x, mov_stat()->pos_actual.y, mov->sec_img.at(mov_stat()->ulti_sec).x, mov->sec_img.at(mov_stat()->ulti_sec).y, mov->sec_img.at(mov_stat()->ulti_sec).w, mov->sec_img.at(mov_stat()->ulti_sec).h);									
			if (m_delay++ == static_cast<int32_t>(mov->retardo))
			{
				int32_t y = mov_stat()->pos_actual.y + mov->guia.at(mov_stat()->ulti_sec).y_accion;
				SimulaMovimiento(mov_stat()->pos_actual.x, y, ImagenDestino->w, ImagenDestino->h);
				ConsolidaMovimiento();
				mov_stat()->ulti_sec++;	
				SetDelay(0);			
			}
		} 
		else
		{
			Imagen::Dibujar(ImagenDestino, Lastmov->imagen, mov_stat()->pos_actual.x, mov_stat()->pos_actual.y, Lastmov->sec_img.at(0).x, Lastmov->sec_img.at(0).y, Lastmov->sec_img.at(0).w, Lastmov->sec_img.at(0).h);			
			GestContMov(TipoMov, Perfil);
		}
	}

	void ElFuler::Dibujar (SDL_Surface* ImagenDestino, int32_t TipoMov)
	{

		if (TipoMov == descanso) // DESPLAZAMIENTO PERPENDICULAR (ACABA COMO EMPIEZA)
			MovSinDespPerp(ImagenDestino, TipoMov, mov_stat()->sentido);

		else if (TipoMov == avance || TipoMov == retorno) // DESPLAZAMIENTO HORIZONTAL (ACABA EN DESCANSO)
			MovDespHorizontal(ImagenDestino, TipoMov, mov_stat()->sentido, descanso);

		else if (TipoMov == salto_arriba || TipoMov == golpe_mano_alto )  // DESPLAZAMIENTO VERTICAL (ACABA EN DESCANSO)
			MovDespVertical(ImagenDestino, TipoMov, mov_stat()->sentido, descanso);	

		else if (TipoMov == patada_helicop || TipoMov == salto_largo ) // DESPLAZAMIENTO HORIZONTAL y VERTICAL (ACABA EN DESCANSO)
			MovDespHorizontalVertical(ImagenDestino, TipoMov, mov_stat()->sentido, descanso);
		
        else if (TipoMov == recep_golpe_N1 || TipoMov == recep_golpe_N2 || TipoMov == recep_golpe_N3 ) // RECEPCION DE GOLPES
            MovRecepGolpe(ImagenDestino, TipoMov, mov_stat()->sentido, descanso);
        
		else if (TipoMov == bola_fuego)  // LANZAMIENTO DE OBJETO (ACABA EN DESCANSO)
			MovDisparo(ImagenDestino, TipoMov, mov_stat()->sentido, descanso);

		else if (TipoMov == patada || TipoMov == golpeo_mano || TipoMov == patada_circular ) // SIN DESPLAZAMIENTO (ACABA EN DESCANSO)
			MovSinDesp(ImagenDestino, TipoMov, mov_stat()->sentido, descanso);

		else if (TipoMov == agacharse || TipoMov == patada_baja || TipoMov ==  golpeo_mano_bajo) // SIN DESPLAZAMIENTO (ACABA AGACHADO)
			MovSinDesp(ImagenDestino, TipoMov, mov_stat()->sentido, agacharse);

	}


	bool ElFuler::CheckMaxKeyPress()
	{
		
		uint32_t KeyPress = 0;
		for(int32_t i = 0; i < SDLMAXKEYS; i++)
		{
			if (teclas(i)->Press == true) KeyPress++;
			if (KeyPress > 2) return true;			
		}
		return false;
	}

	/*
	stringstream tio;
	tio << "Gestion Movimiento Derecho.Press : " << conv::boolToString(keys[SDLK_RIGHT].Press) << " Derecho.UpPress : " << conv::boolToString(keys[SDLK_RIGHT].UnPress);
	Log.Add(tio.str());
	tio.str("");
	*/

	bool ElFuler::GestorMov()
	{
		stringstream iost;    
        
		// CAMBIO PERFIL
        

		if (CheckMaxKeyPress() == true) return true; 
		if (teclas(SDLK_ESCAPE)->Press == true) return false;  
		if (mov_stat()->en_curso == patada_circular || mov_stat()->en_curso == golpe_mano_alto || 
			mov_stat()->en_curso == salto_arriba    || mov_stat()->en_curso == bola_fuego      ||
			mov_stat()->en_curso == patada_helicop  || mov_stat()->en_curso == salto_largo ||
            mov_stat()->en_curso == recep_golpe_N1 || mov_stat()->en_curso == recep_golpe_N2 ||
            mov_stat()->en_curso == recep_golpe_N3 )
			return true;

        
		// CAMBIO PERFIL
        /*
		if (teclas(SDLK_SPACE)->Press == true)
		{
			mov_stat()->sentido = (mov_stat()->sentido == perfilDer) ? perfilIzq : perfilDer;
			SetDelay(0);
			return true;
		}
        */
		/*----------------------------------------------------------------------------------------------------------------------
		*  MOVIMIENTO DE DESPLAZAMIENTO Y QUE PUEDEN SER USADOS CON LA TECLA PULSADA DE CONTINUO
		----------------------------------------------------------------------------------------------------------------------*/
		// RETROCESO
		if (teclas(SDLK_LEFT)->Press == true)
		{	
			ActivaTipoMovimiento(retorno, mov_stat()->sentido);
			mov_stat()->repeticion[retorno] = true;	
			SetDelay(0);	
			return true;
		}
		if (teclas(SDLK_LEFT)->UnPress == true)
		{
			mov_stat()->repeticion[retorno] = false;	
			return true;
		}

		// AVANCE
		if (teclas(SDLK_RIGHT)->Press == true)
		{	
			//Log.Add("GESTORMOV: PRESS RIGHT");
			ActivaTipoMovimiento(avance, mov_stat()->sentido);
			mov_stat()->repeticion[avance] = true;	
			SetDelay(0);	
			return true;
		}  
		if (teclas(SDLK_RIGHT)->UnPress == true)
		{	
			//Log.Add("GESTORMOV: UNPRESS RIGHT");
			mov_stat()->repeticion[avance] = false;				
			return true;
		} 

		// AGACHARSE
		if (teclas(SDLK_DOWN)->Press == true)
		{	
			ActivaTipoMovimiento(agacharse, mov_stat()->sentido);
			mov_stat()->repeticion[agacharse] = true;	
			SetDelay(0);	
			return true;
		}
		if (teclas(SDLK_DOWN)->UnPress == true)
		{
			mov_stat()->repeticion[agacharse] = false;	
			return true;
		}  
	
		/*----------------------------------------------------------------------------------------------------------------------
		* MOVIMIENTOS DE ATAQUE COMPUESTOS UNA TECLA SIEMPRE PERMANECE PULSADA (DEBEN IR SIMPRE ANTES QUE LOS MOVS.ATQ. SIMPES)
		----------------------------------------------------------------------------------------------------------------------*/
		// PATADA BAJA  
		if (mov_stat()->repeticion[agacharse] == true && teclas(SDLK_s)->Press == true)
		{	
			ActivaTipoMovimiento(patada_baja, mov_stat()->sentido);
			mov_stat()->repeticion[patada_baja] = false;	
			SetDelay(0);
			return true;
		}

		// GOLPE MANO BAJO  
		if (mov_stat()->repeticion[agacharse] == true && teclas(SDLK_a)->Press == true)
		{	
			ActivaTipoMovimiento(golpeo_mano_bajo, mov_stat()->sentido);
			mov_stat()->repeticion[golpeo_mano_bajo] = false;	
			SetDelay(0);
			return true;
		}     

		/*----------------------------------------------------------------------------------------------------------------------
		* MOVIMIENTOS DE ATAQUE SIMPLES
		----------------------------------------------------------------------------------------------------------------------*/

		// PUÑETAZO
		if (teclas(SDLK_a)->Press == true)
		{	
			ActivaTipoMovimiento(golpeo_mano, mov_stat()->sentido);
			mov_stat()->repeticion[golpeo_mano] = false;	
			SetDelay(0);
			return true;
		}

		// PATADA
		if (teclas(SDLK_s)->Press == true)
		{	
			ActivaTipoMovimiento(patada, mov_stat()->sentido);
			mov_stat()->repeticion[patada] = false;	
			SetDelay(0);
			return true;
		}  

		// PATADA CIRCULAR
		if (teclas(SDLK_d)->Press == true)
		{	
			ActivaTipoMovimiento(patada_circular, mov_stat()->sentido);
			mov_stat()->repeticion[patada_circular] = false;	
			SetDelay(0);
			return true;
		}

		// BOLA DE BUEGO
		if (teclas(SDLK_z)->Press == true )
		{	
			ActivaTipoMovimiento(bola_fuego, mov_stat()->sentido);
			mov_stat()->repeticion[bola_fuego] = false;	
			SetDelay(0);
			return true;
		}  
		
		// SALTO
		if (teclas(SDLK_UP)->Press == true)
		{	
			ActivaTipoMovimiento(salto_arriba, mov_stat()->sentido);
			mov_stat()->repeticion[salto_arriba] = false;	
			SetDelay(0);	
			return true;
		}

		// GOLPE MANO ALTO  
		if (teclas(SDLK_x)->Press == true)
		{	
			ActivaTipoMovimiento(golpe_mano_alto, mov_stat()->sentido);
			mov_stat()->repeticion[golpe_mano_alto] = false;	
			SetDelay(0);
			return true;
		}  

		// PATADA HELICOPTERO  
		if (teclas(SDLK_c)->Press == true)
		{	
			ActivaTipoMovimiento(patada_helicop, mov_stat()->sentido);
			mov_stat()->repeticion[patada_helicop] = false;	
			SetDelay(0);
			return true;
		} 

		// SALTO LARGO  
		if (teclas(SDLK_f)->Press == true)
		{	
			ActivaTipoMovimiento(salto_largo, mov_stat()->sentido);
			mov_stat()->repeticion[salto_largo] = false;	
			SetDelay(0);
			return true;
		} 

		return true;
	}

} // namespace bp
