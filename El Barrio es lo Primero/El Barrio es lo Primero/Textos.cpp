#include "Textos.hpp"

namespace bp
{


Texto::Texto(void) :

	m_fuente(0)

{

    m_color.r = 255;
    m_color.g = 255;
    m_color.b = 255;
    m_colorFondo.r = 0;
    m_colorFondo.g = 0;
    m_colorFondo.b = 0;
}

Texto::~Texto()
{
  Limpiar();
}

void Texto::Limpiar()
{
    if (m_fuente)
        TTF_CloseFont(m_fuente);
}

bool Texto::CargaFuente(const char* rutaFichero, int32_t alto)
{
    m_fuente = TTF_OpenFont(rutaFichero, alto);
    if (m_fuente == 0)
    {        
		SDL_SetError(TTF_GetError());        
		return false;
    }
    
     return true;
}

SDL_Surface* Texto::Dibujar(const char* texto)
{
    SDL_Surface* ImagenDestino = 0;    
    ImagenDestino = TTF_RenderUTF8_Shaded(m_fuente, texto, m_color, m_colorFondo);
    if (ImagenDestino == 0)
    {
		SDL_SetError(TTF_GetError());        
		return 0;
    }

    return ImagenDestino;
}

void Texto::PonerColor(SDL_Color color)
{
    m_color.r = color.r;
    m_color.g = color.g;
    m_color.b = color.b;
}

void Texto::PonerColorFondo(SDL_Color colorFondo)
{
    m_colorFondo.r = colorFondo.r;
    m_colorFondo.g = colorFondo.g;
    m_colorFondo.b = colorFondo.b;
}

} // namespace bp
