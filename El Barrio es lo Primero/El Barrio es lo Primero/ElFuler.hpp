#ifndef ELFULER_HPP
#define ELFULER_HPP

/*--------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------*/
#include "utils.hpp"
#include "actor.hpp"

namespace bp
{

/*--------------------------------------------------------------------------------------------------------------
 * CLASS : FULLER
 *-------------------------------------------------------------------------------------------------------------*/
class ElFuler : public actor
{
	/*--------------------------------------------------------------------------------------------------------------
	* PRIVATE ATTRIBUTES
	*--------------------------------------------------------------------------------------------------------------*/	
	private:				
		int32_t														m_desp_x, m_desp_y, m_perfil;
        actor*                                                      m_enemy;
		
	/*--------------------------------------------------------------------------------------------------------------
	* PRIVATE ENUMS
	*--------------------------------------------------------------------------------------------------------------*/	
	private:	
		enum													{ ALTURA_SPRITE = 200 };

	/*--------------------------------------------------------------------------------------------------------------
	* PRIVATE METHODS
	*--------------------------------------------------------------------------------------------------------------*/		
	private:		
		/* MOVIMIENTOS
		*---------------------------------------------------------------------------------------------------------------------------------------
		* 200 pixeles el maximo de altura permitida por lo que todas las imagenes deben estar alineadas a la 
		* base de este limite en 200 px. Sumandose 200px por cada secuencia de sprites.(HAY ALGUNA EXCEPCION; PERO SE DEBE TOMAR ESTO COMO REGLA)
		* Como Ejemplo
		* Descanso.y  = 0
		* Avanza.Y    = 200
		* Retroceso.Y = 400
		* .... ULTIMO.MOVIMIENTO.Y * 200.	
		*/		
		void						CargaMovDescanso				();		
		void						CargaMovAvanza					();
		void						CargaMovRetrocede				();
		void						CargaPatada						();
		void						CargaGolpeMano					();
		void						CargaAgacharse					();
		void						CargaSalto						();	
		void						CargaPatadaCircular				();
		void						CargaPatadaBaja					();
		void						CargarGolpeManoBajo				();
		void						CargarGolpeManoAlto				();
		void						CargaBolaFuego					();
		void						CargarSaltoLargo				();
		void						CargarPatadaHelocop				();
        void                        cargarRecepGolpeN1              ();
        void                        cargarRecepGolpeN2              ();
        void                        cargarRecepGolpeN3              ();
    
		/*--------------------------------------------------------------------------------------------------------------------------------------*/
		
		bool						GestorMov						();
		void						GestContMov						(CoInt_t TipoMov, CoInt_t Perfil);
		bool						CheckMaxKeyPress				();
		void						MovSinDespPerp					(SDL_Surface* ImagenDestino, int32_t TipoMov, int32_t Perfil);
		void						MovDespHorizontal				(SDL_Surface* ImagenDestino, int32_t TipoMov, int32_t Perfil, int32_t LastMov);
        void						MovRecepGolpe                   (SDL_Surface* ImagenDestino, int32_t TipoMov, int32_t Perfil, int32_t LastMov);
		void						MovDespHorizontalVertical		(SDL_Surface* ImagenDestino, int32_t TipoMov, int32_t Perfil, int32_t LastMov);
		void						MovDespVertical					(SDL_Surface* ImagenDestino, int32_t TipoMov, int32_t Perfil, int32_t LastMov);
		void						MovSinDesp						(SDL_Surface* ImagenDestino, int32_t TipoMov, int32_t Perfil, int32_t LastMov);	
		void						MovDisparo						(SDL_Surface* ImagenDestino, int32_t TipoMov, int32_t Perfil, int32_t LastMov);

	/*--------------------------------------------------------------------------------------------------------------
	* PUBLIC METHODS
	*--------------------------------------------------------------------------------------------------------------*/		
	public:
									ElFuler						(SDL_Rect PosInicial, int32_t Perfil, Color_t colorTransp, const char* ruta_perfil_der, const char* ruta_perfil_izq, int32_t vida = 100);
									~ElFuler					(void) {}
		void						Dibujar						(SDL_Surface* ImagenDestino, int32_t TipoMovimiento);		
		
};

} // namespace bp

#endif // ELFULER_HPP
