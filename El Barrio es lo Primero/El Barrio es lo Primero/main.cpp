#include "main.hpp"

int main(int , char** )
{
	bp::Log.Add("Inicia Juego.");	
	bp::Carg_Juego juego("CARGADOR_JUEGO");        
	if ((juego.Ejecutar()) == false)    
	{
		bp::Log.Add("Finaliza Juego ERROR.");
		std::cerr << SDL_GetError() << std::endl;
        return 1;
    }
    bp::Log.Add("Finaliza Juego OK.");

	return 0;
}
