/* MOD FOR C++ OF:
    SDL_collide:  A 2D collision detection library for use with SDL
    Copyright (C) 2005 Amir Taaki

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Amir Taaki
    genjix@gmail.com

    Rob Loach
    http://robloach.net
*/
#ifndef COLISIONES_HPP
#define COLISIONES_HPP

#include "utils.hpp"

/*returns maximum or minimum of number*/
#define SDL_COLLIDE_MAX(a,b)	((a > b) ? a : b)
#define SDL_COLLIDE_MIN(a,b)	((a < b) ? a : b)

namespace bp
{

class Colisiones
{
  public:
	static bool			CollideTransparentPixelTest				(SDL_Surface* surface , int32_t u , int32_t v);	 // SDL surface test if offset (u,v) is a transparent pixel
	static bool			CollidePixel							(SDL_Surface* as ,int32_t ax ,int32_t ay ,SDL_Surface* bs ,int32_t bx ,int32_t by); // SDL pixel perfect collision test	
	static bool			CollideBoundingBox						(SDL_Surface* sa ,int32_t ax ,int32_t ay ,SDL_Surface* sb ,int32_t bx ,int32_t by); // SDL bounding box collision test	
	static bool			CollideBoundingBox						(SDL_Rect a ,SDL_Rect b); // SDL bounding box collision tests (works on SDL_Rect's)
	/*
	tests whether 2 circles intersect

	circle1 : centre (x1,y1) with radius r1
	circle2 : centre (x2,y2) with radius r2
	
	(allow distance between circles of offset)
	*/
	static bool			CollideBoundingCircle					(int32_t x1, int32_t y1, int32_t r1, int32_t x2, int32_t y2 ,int32_t r2 ,int32_t offset);
	/*
	a circle intersection detection algorithm that will use
	the position of the centre of the surface as the centre of
	the circle and approximate the radius using the width and height
	of the surface (for example a rect of 4x6 would have r = 2.5).
	*/	
	static bool			CollideBoundingCircle					(SDL_Surface *a ,int32_t x1 ,int32_t y1, SDL_Surface* b ,int32_t x2 ,int32_t y2 , int32_t offset);
	
};

}
#endif // COLISIONES_HPP
