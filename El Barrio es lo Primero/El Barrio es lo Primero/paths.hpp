#ifndef PATHS_HPP
#define PATHS_HPP

#include "tipos_glob.hpp"

class paths
{
    private:
        
        // ATRIBUTOS
        std::string					m_fullpath; // Path with filename, if exists.
        std::string					m_path; // Path without filename, if exits.
        std::string					m_fullfilename;
        std::string					m_filename;        
        std::string					m_extension;
        std::vector<int32_t>			m_levels;
        bool						m_directory, m_file, m_exists;
        char						sep;
        enum						kind {directory, file};
        
        // METODOS
        void						getLevels				();        
        void						extractFilename			();
        void						extractExtension		();        
        int32_t							whatKindOf				();
		std::string					getRootPath				()	{ return boost::filesystem::current_path().string(); }
		void						extractPath				() // Path without filename, if exits.
        {
            assert(m_levels.size() > 0);            
            m_path = (m_file == true)? m_fullpath.substr(0, m_levels[m_levels.size() - 1]) : m_fullpath;
        }
        
    public:
        
									paths					(); // Si no lleva parametro usa el ROOTPATH = "Path completo del Ejecutable en uso"
									paths					(std::string ipath);
									~paths					() {}
        std::string					GetFullPath				() { return m_fullpath; }
        std::string					GetPath					() { return m_path; }
        std::string					GetFilename				() { return m_filename; }
        std::string					GetFullFilename			() { return m_fullfilename; }
        std::string					GetExtension			() { return m_extension; }
        bool						IsDirectory				() { return m_directory; }
        bool						IsFile					() { return m_file; }
        bool						HasExtension			() { return (m_extension.empty())? false : true; }                             
        const std::string			ToString				();
        bool						Exists					() { return m_exists; }
        std::vector<std::string>	ListFiles				(std::string ipath);
		std::string					GetPath					(int32_t pos)
        {
            assert(pos <= m_levels.size());
            return  m_fullpath.substr(0, m_levels[m_levels.size() - pos]);
        }
};

#endif // PATHS_HPP
