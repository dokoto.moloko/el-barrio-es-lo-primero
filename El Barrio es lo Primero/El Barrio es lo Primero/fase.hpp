#ifndef FASE_HPP
#define FASE_HPP

#include "cargador.hpp"
#include "configurador.hpp"

namespace bp
{
  
class fase : public cargador
{	
	public:	
							fase				(const char* id) : cargador(id) {}
							fase				(const char* id, configurador* config) : cargador(id, config) {}
		virtual				~fase				(void) {}

		virtual bool 		Ejecutar			() { return true; } 
		virtual bool		iteracion			() { return true; } 
		virtual bool		Inicializar			() { return true; }
        virtual	void		GestionaMovs		(Keys_t []) {}
		virtual void		Limpiar				() {}
};

}
#endif //FASE_HPP
