#ifndef FASE_CONFIG_HPP
#define FASE_CONFIG_HPP

/*--------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------*/
#include "utils.hpp"
#include "Eventos.hpp"
#include "fase.hpp"
#include "Imagen.hpp"
#include "Textos.hpp"
#include "SDL_FrameRate.hpp"
#include "Musica.hpp"

namespace bp
{
  
class Fase_Config : public Eventos, public fase
{
	/*--------------------------------------------------------------------------------------------------------------
	* PRIVATE ENUMS
	*--------------------------------------------------------------------------------------------------------------*/			
	private:		
		enum 													{ m_ancho = 3, m_alto = 5 };
		enum													{ fuler, benedicto };

	/*--------------------------------------------------------------------------------------------------------------
	* PRIVATE ATTRIBUTES
	*--------------------------------------------------------------------------------------------------------------*/			
	private:	
		FPSmanager												m_manejadorImgPorSeg;
		bool													m_ejecutandose;
		UShort_t												m_p_ancho;
		UShort_t												m_p_alto;
		UShort_t												mo_menu;
		UShort_t												mo_submenu;
		CoShort_t												m_borde;
		CoShort_t												m_retrato;
		CoShort_t												m_recuadro;
		SDL_Surface*											m_fondo;
		SDL_Surface*											m_desconocido;
		SDL_Surface*											m_fondo_menu;
		const int32_t												m_ancho_retrato;
		Short_t													m_vigila_desp_vertical; // -1 : Abajo 0 : estatico 1 : arriba
		bool													m_menu_oculto_activo;
		SDL_Event												m_evento;		
		std::vector<SDL_Surface*>								m_personajes;	  		
		UShort_t												m_cuadricula[m_ancho][m_alto];								
		std::vector<Mix_Music*>									m_personajes_sonido;	  
		bool													m_enciende_clave;
		std::vector<SDLKey>										m_clave_entrada;
		std::vector<SDLKey>										m_clave;				
		Texto													m_tx_titulo_select_personajes;
		Texto													m_tx_linea_menu_oculto;
		Texto													m_tx_titulo_menu_oculto;
		Texto													m_tx_opcion_menu;		
		Keys_t													m_keys[SDLMAXKEYS];
		bool													m_continuar_juego;
	  
	  
	/*--------------------------------------------------------------------------------------------------------------
	* PRIVATE METHODS
	*--------------------------------------------------------------------------------------------------------------*/			
	private:
		bool						Inicializar					();
		bool						Iteracion					();
		bool						Dibujar						();
		bool						Sonar						();
		void						Limpiar						();
		void	 					Salir						();
		void						OnEvent						(SDL_Event* Evento);
		void						OnKeyDown					(SDLKey sym, SDLMod , Uint16  );  
		void						OnKeyUp						(SDLKey sym, SDLMod, Uint16);
		bool						PersonajeSeleccionado		();
		void 						DibujarPersonajes			(SDL_Surface* ImagenDestino);
		void						MenuSelecPersonajes			(SDL_Surface* ImagenDestino);
		void						TextoMenuPersonajes			(SDL_Surface* ImagenDestino);
		void 						KeyDownMenuSelecPersonajes	(Keys_t movs[]);
		void						KeyDownMenuOculto			(Keys_t movs[]);	  
		void						DibujarMenuOculto			(SDL_Surface* ImagenDestino);
		void						AplicaConfig				();
		void						GestionaMovs				(Keys_t movs[]);

	/*--------------------------------------------------------------------------------------------------------------
	* PUBLIC METHODS
	*--------------------------------------------------------------------------------------------------------------*/			
	public:		
									Fase_Config					(const char* id, configurador* config);
									~Fase_Config				(void);			
		bool						Ejecutar					();					
};

} // namespace bp


#endif // FASE_CONFIG_HPP
