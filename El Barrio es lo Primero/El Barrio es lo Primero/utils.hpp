#ifndef UTILS_HPP
#define UTILS_HPP

/*--------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------*/
#include "tipos_glob.hpp"

#define TO_STRING( name ) # name


namespace bp 
{
namespace utl
{
namespace file
{
		/*--------------------------------------------------------------------------------------------------------------
		* ENUMS
		*--------------------------------------------------------------------------------------------------------------*/		
		enum													{fichero, directorio, otros, todos};

		/*--------------------------------------------------------------------------------------------------------------
		* FUNCTIONS
		*--------------------------------------------------------------------------------------------------------------*/		
		bool					Split							(CoChar_t& sep, CoCStr_t raw, VStr_t& split_values);
		CoStr_t					CurrentDir						(void);
		uint32_t					NumOfDirLevels					(void);
		Char_t					GetDirSep						(void);
		CoStr_t					DirLevel						(const size_t& Level);
		CoStr_t					FixWinPath						(CoCStr_t path);	
		CoStr_t					ExtractExt						(CoCStr_t path);
		bool					RemoveFile						(CoCStr_t path);
		bool					ListDir							(CoCStr_t path, CoUInt_t filter, VFile_t& list);
		bool					CreateDir						(CoCStr_t path);					
		bool					ExistDir						(CoCStr_t path);
		bool					ReadTxFile						(CoCStr_t path, Str_t& content);
		bool					ReadBinFile						(CoCStr_t path, Str_t& content);

} // namespace file

namespace sys
{
		/*--------------------------------------------------------------------------------------------------------------
		* FUNCTIONS
		*--------------------------------------------------------------------------------------------------------------*/		
		bool					ExecCmd							(CoCStr_t cmd);
}



} // namespace utl

/*--------------------------------------------------------------------------------------------------------------
123* CLASS : CONV
*--------------------------------------------------------------------------------------------------------------*/  
class conv
{
  
	/*--------------------------------------------------------------------------------------------------------------
	* PUBLIC STATIC METHODS		
	*--------------------------------------------------------------------------------------------------------------*/
	public:
		static const char*		boolToString					(bool value) { return (value) ? "TRUE" : "FALSE" ; }
		static Sint16			sSint16toUint16					(Sint16 x, Uint16 y) { return static_cast<Sint16>( x + static_cast<Sint16>(y)); }
		static const char*		sdlkeytostring					(int32_t key); 
		static const char*		SDLKeyToString					(SDLKey key) { return sdlkeytostring(static_cast<int32_t>(key)); }
 		static const char*		SDLKeyToString					(Uint8 key) { return sdlkeytostring(static_cast<int32_t>(key)); }
		static SDLKey			KeyToSDLKey						(int32_t key)
		{
			if (key == golpeo_mano)
				return SDLK_a;
			if (key == patada)
				return SDLK_s;
			if (key == patada_circular)	
				return SDLK_d;
			if (key == golpe_mano_alto)
				return SDLK_x;
			if (key == bola_fuego)
				return SDLK_z;
			if (key == avance)
				return SDLK_RIGHT;
			if (key == retorno)
				return SDLK_LEFT;		
			if (key == agacharse)
				return SDLK_DOWN;
			if (key == salto_arriba)
				return SDLK_UP;
			return SDLK_ESCAPE;
		}

};

/*--------------------------------------------------------------------------------------------------------------
* CLASS : Log
*--------------------------------------------------------------------------------------------------------------*/
class clLog
{
	/*--------------------------------------------------------------------------------------------------------------
	* PRIVATE ATTRIBUTES
	*--------------------------------------------------------------------------------------------------------------*/
	private:
		std::ofstream											m_Log;		
	
	/*--------------------------------------------------------------------------------------------------------------
	* PUBLIC METHODS
	*--------------------------------------------------------------------------------------------------------------*/
	public:
								clLog							(const char* ruta = "traze.Log");
								~clLog							();
	void						Add								(std::string tx);
	
};


/*--------------------------------------------------------------------------------------------------------------
* CLASS : ROOTPATH
*--------------------------------------------------------------------------------------------------------------*/
class clPaths
{
	/*--------------------------------------------------------------------------------------------------------------
	* PRIVATE ATTRIBUTES
	*--------------------------------------------------------------------------------------------------------------*/
	private:
		Str_t													m_ruta_raiz;
		MStr_t													m_conf_rutas;
		Char_t													m_sep;

	/*--------------------------------------------------------------------------------------------------------------
	* PRIVATE METHODS
	*--------------------------------------------------------------------------------------------------------------*/
	private:
		void					CargaRutas						();
		Str_t					DefineRutaRaiz					();

	/*--------------------------------------------------------------------------------------------------------------
	* PUBLIC METHODS
	*--------------------------------------------------------------------------------------------------------------*/	
	public:
								clPaths							();
								~clPaths						() {}
		Str_t					ObtieneRuta						(CoStr_t clave)	{ return m_conf_rutas[clave]; }
		CoCStr_t				ObtieneRuta						(CoCStr_t clave){ return m_conf_rutas[Str_t(clave)].c_str(); }
		

};


/*--------------------------------------------------------------------------------------------------------------
* EXTERN GLOB VARS
*--------------------------------------------------------------------------------------------------------------*/
extern clPaths Paths;
extern clLog Log;

} // namespace bp
#endif // UTILS_HPP
