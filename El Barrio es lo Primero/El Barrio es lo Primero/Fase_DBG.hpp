#ifndef FASE_01_HPP
#define FASE_01_HPP

/*--------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------*/
#include "colisiones.hpp"
#include "actor.hpp"
#include "proyectil.hpp"
#include "ElFuler.hpp"
#include "Eventos.hpp"
#include "Imagen.hpp"
#include "fase.hpp"
#include "Textos.hpp"
#include "configurador.hpp"
#include "SDL_FrameRate.hpp"
#include "fx.hpp"
#define TO_STRING( name ) # name

namespace bp
{

/*--------------------------------------------------------------------------------------------------------------
 * CLASS : Fase_DBG (Eventos, Fase)
 *-------------------------------------------------------------------------------------------------------------*/
class Fase_DBG : public Eventos, public fase
{
  
	/*--------------------------------------------------------------------------------------------------------------
	* PRIVATE ATTRIBUTES
	*--------------------------------------------------------------------------------------------------------------*/
	private:
		FPSmanager												m_manejadorImgPorSeg;
		bool													m_ejecutandose;
		SDL_Event												m_evento;		
		SDL_Surface*											m_fondo;	
		ElFuler*												m_enemigo;
		std::vector<Proyectil*>									m_proyectiles;		
		FX														fx[2];
		Uint32													GetTime;
		Uint32													tiempoEntreMovs[TipoMov_LEN];
        enum                                                    {HEROE, ENEMIGO, TABLAS};
	  
	  // DEBUG
	  /*----------------------------------------------------------------------------------------------------------------------------------------------------*/
		Texto													m_tx_Debg, m_tx_Debg2;
		std::stringstream 										m_io, m_io2, m_io3;			  
	  /*----------------------------------------------------------------------------------------------------------------------------------------------------*/
	  	  
	  
	/*--------------------------------------------------------------------------------------------------------------
	* PRIVATE METHODS
	*--------------------------------------------------------------------------------------------------------------*/
	private:
		//virtual bool				GestorMov					() { return true; }
	  // DEBUG
	  /*----------------------------------------------------------------------------------------------------------------------------------------------------*/
		void 						Pgdb						(SDLKey sym, const char* atr, bool val); 
	  /*----------------------------------------------------------------------------------------------------------------------------------------------------*/
	  
		bool						Inicializar					();
		bool						Iteracion					();
		bool						Dibujar						();
		void						Limpiar						();
		void 						Salir						();
 		void						OnEvent						(SDL_Event* Evento) { Eventos::OnEvent ( Evento ); }	  	  
		void						OnKeyDown					(SDLKey sym, SDLMod , Uint16 );
		void						OnKeyUp						(SDLKey sym, SDLMod, Uint16);
		void						GestionMovs					() { if ( (m_configurador->ObtenerActor()->GestorMov()) == false ) Salir(); }
		void						GestionMovsAI				(SDL_Surface* Escenario, actor* heroe, actor* enemigo);
		bool						EscanerColisiones			(SDL_Surface* Escenario, actor* heroe, ty_s_conf_mov* mov_heroe, actor* enemigo, ty_s_conf_mov* mov_enemigo, int32_t& quienGolpeoPrimero);
		void	 					Camara						(SDL_Surface* ImagenDestino, SDL_Surface* ImagenOrigen);
        void	 					Camara2						(SDL_Surface* ImagenDestino, SDL_Surface* ImagenOrigen);
    
		void						ObtAreaPersonajes			(actor* heroe, ty_s_conf_mov* mov_heroe, actor* enemigo, ty_s_conf_mov* mov_enemigo, SDL_Rect& rect_heroe, SDL_Rect& rect_enemigo);
		void						GestEnergia					(actor* heroe, ty_s_conf_mov* mov_heroe, actor* enemigo, ty_s_conf_mov* mov_enemigo, SDL_Rect rect_heroe, SDL_Rect rect_enemigo, int32_t& quienGolpeoPrimero);
		//void						RespGolpe					(actor* heroe, ty_s_conf_mov* mov_heroe, actor* enemigo, ty_s_conf_mov* mov_enemigo, SDL_Rect rect_heroe, SDL_Rect rect_enemigo);
		void						GestFXs						(actor* heroe, actor* enemigo);

	/*--------------------------------------------------------------------------------------------------------------
	* PRIVATE METHODS
	*--------------------------------------------------------------------------------------------------------------*/
	public:

									Fase_DBG					(const char* id, configurador* config);
									~Fase_DBG					(void);					
		 bool						Ejecutar					();  
  	  	 					
};

} // namespace bp

#endif // FASE_01_HPP
