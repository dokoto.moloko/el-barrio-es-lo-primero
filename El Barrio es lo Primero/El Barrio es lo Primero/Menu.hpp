#ifndef MENU_HPP
#define MENU_HPP

/*--------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------*/
#include "utils.hpp"

namespace bp
{
 

/*--------------------------------------------------------------------------------------------------------------
 * PUBLIC TYPES
 *-------------------------------------------------------------------------------------------------------------*/
typedef std::pair<std::string, unsigned>		ty_id;
typedef std::string								ty_titulo;
typedef struct									ty_s_menu
{
	ty_id						idMenuSig;	
	ty_id						idMenuAnt;		
	ty_titulo					titulo;
	std::string					valor;
	bool						estaActivo;
	bool						esDirectorio;		

}												ty_s_menu;
typedef std::map<ty_id, ty_s_menu>				ty_m_menu;
typedef std::map<ty_id, ty_s_menu>::iterator	ty_m_it_menu;


/*--------------------------------------------------------------------------------------------------------------
 * CLASS: Menu
 *-------------------------------------------------------------------------------------------------------------*/
class Menu
{
	/*--------------------------------------------------------------------------------------------------------------
	* PRIVATE ATTRIBUTES
	*--------------------------------------------------------------------------------------------------------------*/
	private:
		ty_m_menu												m_root_menu;
		ty_id													m_root;
		ty_m_it_menu											m_it_nodo;
	
	/*--------------------------------------------------------------------------------------------------------------
	* PUBLIC METHODS
	*--------------------------------------------------------------------------------------------------------------*/
	public:
									Menu						(ty_id root) : m_root(root) {}
									~Menu						(void) {}
		void						NuevoMenu					(ty_id id, ty_id idMenuAnt, ty_titulo titulo, std::string valor, bool estaActivo, bool esDirectorio);
		bool						IrInicio					();
		bool						AvanArriba					();
		bool						AvanAbajo					();
		bool						AvanDer						();
		bool						AvanIzq						();
		void						PoneValor					(std::string valor) { (*m_it_nodo).second.valor = valor; }
		ty_m_it_menu				ObtMenuActual				() { return m_it_nodo; }
		ty_m_it_menu				ObtMenu						(ty_id id) { return m_root_menu.find(id); }
		ty_m_it_menu				Iterar						();
		ty_m_it_menu				Inicio						() { return m_root_menu.begin(); }
		ty_m_it_menu				Fin							() { return m_root_menu.end(); }
		bool						EsLaActual					(ty_m_it_menu it_in) { return (it_in == m_it_nodo) ? true : false; }

};

} // namespace bp

#endif //MENU_HPP
