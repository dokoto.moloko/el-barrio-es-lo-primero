#ifndef IMAGEN_HPP
#define IMAGEN_HPP

/*--------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------*/
#include "utils.hpp"

namespace bp
{

/*--------------------------------------------------------------------------------------------------------------
 * CLASS : IMAGEN
 *-------------------------------------------------------------------------------------------------------------*/
class Imagen
{

	/*--------------------------------------------------------------------------------------------------------------
	* PUBLIC METHODS
	*--------------------------------------------------------------------------------------------------------------*/
	public:		
		static SDL_Surface*			Cargar						(const char* RutaFichero);
		static bool					Dibujar						(SDL_Surface* ImagenDestino, SDL_Surface* ImagenOrigen, int32_t x = 0, int32_t y = 0);
		static bool					Dibujar						(SDL_Surface* ImagenDestino, SDL_Surface* ImagenOrigen, int32_t PosicionX, int32_t PosicionY, int32_t PosicionRecorteX, int32_t PosicionRecorteY, uint16_t ancho, uint16_t alto);
        static bool					Dibujar						(SDL_Surface* ImagenDestino, SDL_Surface* ImagenOrigen, int32_t PosicionX, int32_t PosicionY, int32_t PosicionRecorteX, int32_t PosicionRecorteY, int ancho, int alto);
		static bool					DibujarRectangulo			(SDL_Surface* ImagenDestino, int32_t x, int32_t y, uint16_t ancho, uint16_t alto, Uint32 color);
		static bool					DibujarRectangulo			(SDL_Surface* ImagenDestino, int32_t x, int32_t y, int ancho, int alto, Uint32 color);
		static void					CrearTransparencia			(SDL_Surface*& ImagenDestino, const Uint8 R, const Uint8 G, const Uint8 B);
 		static SDL_Surface*			CrearImagen					(Uint32 opciones, int32_t ancho, int32_t alto, const SDL_Surface* Imagen);
		static void					LimpiarImagen				(SDL_Surface* ImgenDestino);
		static SDL_Rect**			ObtenerResoluciones			() { return SDL_ListModes(NULL, SDL_FULLSCREEN|SDL_HWSURFACE); }
	};

} // namespace bp

#endif // IMAGEN_HPP
