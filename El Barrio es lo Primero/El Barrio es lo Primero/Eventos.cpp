#include "Eventos.hpp"

namespace bp
{

Eventos::Eventos() {
}
 
Eventos::~Eventos() {
    //Do nothing
}
 
void Eventos::OnEvent(SDL_Event* Event) {
    switch(Event->type) {
        case SDL_ACTIVEEVENT: {
            switch(Event->active.state) {
                case SDL_APPMOUSEFOCUS: {
                    if ( Event->active.gain )    
						OnMouseFocus();
                    else                
						OnMouseBlur();
 
                    break;
                }
                case SDL_APPINPUTFOCUS: {
                    if ( Event->active.gain )    
						OnInputFocus();
                    else                
						OnInputBlur();
 
                    break;
                }
                case SDL_APPACTIVE:    {
                    if ( Event->active.gain )    
						OnRestore();
                    else                
						OnMinimize();
 
                    break;
                }
            }
            break;
        }
 
        case SDL_KEYDOWN: {
            OnKeyDown(Event->key.keysym.sym,Event->key.keysym.mod,Event->key.keysym.unicode);
            break;
        }
 
        case SDL_KEYUP: {
            OnKeyUp(Event->key.keysym.sym,Event->key.keysym.mod,Event->key.keysym.unicode);
            break;
        }
 
        case SDL_MOUSEMOTION: {
            OnMouseMove(Event->motion.x,Event->motion.y,Event->motion.xrel,Event->motion.yrel,(Event->motion.state&SDL_BUTTON(SDL_BUTTON_LEFT))!=0,(Event->motion.state&SDL_BUTTON(SDL_BUTTON_RIGHT))!=0,(Event->motion.state&SDL_BUTTON(SDL_BUTTON_MIDDLE))!=0);
            break;
        }
 
        case SDL_MOUSEBUTTONDOWN: {
            switch(Event->button.button) {
                case SDL_BUTTON_LEFT: {
                    OnLButtonDown(Event->button.x,Event->button.y);
                    break;
                }
                case SDL_BUTTON_RIGHT: {
                    OnRButtonDown(Event->button.x,Event->button.y);
                    break;
                }
                case SDL_BUTTON_MIDDLE: {
                    OnMButtonDown(Event->button.x,Event->button.y);
                    break;
                }
            }
            break;
        }
 
        case SDL_MOUSEBUTTONUP:    {
            switch(Event->button.button) {
                case SDL_BUTTON_LEFT: {
                    OnLButtonUp(Event->button.x,Event->button.y);
                    break;
                }
                case SDL_BUTTON_RIGHT: {
                    OnRButtonUp(Event->button.x,Event->button.y);
                    break;
                }
                case SDL_BUTTON_MIDDLE: {
                    OnMButtonUp(Event->button.x,Event->button.y);
                    break;
                }
            }
            break;
        }
 
        case SDL_JOYAXISMOTION: {
            OnJoyAxis(Event->jaxis.which,Event->jaxis.axis,Event->jaxis.value);
            break;
        }
 
        case SDL_JOYBALLMOTION: {
            OnJoyBall(Event->jball.which,Event->jball.ball,Event->jball.xrel,Event->jball.yrel);
            break;
        }
 
        case SDL_JOYHATMOTION: {
            OnJoyHat(Event->jhat.which,Event->jhat.hat,Event->jhat.value);
            break;
        }
        case SDL_JOYBUTTONDOWN: {
            OnJoyButtonDown(Event->jbutton.which,Event->jbutton.button);
            break;
        }
 
        case SDL_JOYBUTTONUP: {
            OnJoyButtonUp(Event->jbutton.which,Event->jbutton.button);
            break;
        }
 
        case SDL_QUIT: {
            OnExit();
            break;
        }
 
        case SDL_SYSWMEVENT: {
            //Ignore
            break;
        }
 
        case SDL_VIDEORESIZE: {
            OnResize(Event->resize.w,Event->resize.h);
            break;
        }
 
        case SDL_VIDEOEXPOSE: {
            OnExpose();
            break;
        }
 
        default: {
            OnUser(Event->user.type,Event->user.code,Event->user.data1,Event->user.data2);
            break;
        }
    }
}

void Eventos::OnInputFocus() {
    //Pure virtual, do nothing
}
 
void Eventos::OnInputBlur() {
    //Pure virtual, do nothing
}
 
void Eventos::OnKeyDown(SDLKey , SDLMod , Uint16 ) {
    //Pure virtual, do nothing
}
 
void Eventos::OnKeyUp(SDLKey , SDLMod , Uint16 ) {
    //Pure virtual, do nothing
}
 
void Eventos::OnMouseFocus() {
    //Pure virtual, do nothing
}
 
void Eventos::OnMouseBlur() {
    //Pure virtual, do nothing
}
 
void Eventos::OnMouseMove(int32_t , int32_t , int32_t , int32_t , bool ,bool ,bool ) {
    //Pure virtual, do nothing
}
 
void Eventos::OnMouseWheel(bool , bool ) {
    //Pure virtual, do nothing
}
 
void Eventos::OnLButtonDown(int32_t , int32_t ) {
    //Pure virtual, do nothing
}
 
void Eventos::OnLButtonUp(int32_t , int32_t ) {
    //Pure virtual, do nothing
}
 
void Eventos::OnRButtonDown(int32_t , int32_t ) {
    //Pure virtual, do nothing
}
 
void Eventos::OnRButtonUp(int32_t , int32_t ) {
    //Pure virtual, do nothing
}
 
void Eventos::OnMButtonDown(int32_t , int32_t ) {
    //Pure virtual, do nothing
}
 
void Eventos::OnMButtonUp(int32_t , int32_t ) {
    //Pure virtual, do nothing
}
 
void Eventos::OnJoyAxis(Uint8 ,Uint8 ,Sint16 ) {
    //Pure virtual, do nothing
}
 
void Eventos::OnJoyButtonDown(Uint8 ,Uint8 ) {
    //Pure virtual, do nothing
}
 
void Eventos::OnJoyButtonUp(Uint8 ,Uint8 ) {
    //Pure virtual, do nothing
}
 
void Eventos::OnJoyHat(Uint8 ,Uint8 ,Uint8 ) {
    //Pure virtual, do nothing
}
 
void Eventos::OnJoyBall(Uint8 ,Uint8 ,Sint16 ,Sint16 ) {
    //Pure virtual, do nothing
}
 
void Eventos::OnMinimize() {
    //Pure virtual, do nothing
}
 
void Eventos::OnRestore() {
    //Pure virtual, do nothing
}
 
void Eventos::OnResize(int32_t ,int32_t ) {
    //Pure virtual, do nothing
}
 
void Eventos::OnExpose() {
    //Pure virtual, do nothing
}
 
void Eventos::OnExit() {
    //Pure virtual, do nothing
}
 
void Eventos::OnUser(Uint8 , int32_t , void* , void* ) {
    //Pure virtual, do nothing
}
} // namespace bp
