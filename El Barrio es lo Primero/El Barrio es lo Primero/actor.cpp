#include "actor.hpp"

namespace bp
{

using namespace std;

actor::actor(const char* nombre, int32_t perfil, SDL_Rect PosInicial, const int32_t vida) :
	
	m_nombre(nombre)

{	
	m_imagen[perfilDer] = 0; 
	m_imagen[perfilIzq] = 0;
	
	m_mov_stat.en_curso = descanso;
	m_mov_stat.sentido = perfil;	
	m_mov_stat.pos_actual.x = PosInicial.x;
	m_mov_stat.pos_actual.y = PosInicial.y;
	m_mov_stat.pos_futura.x = 0;
	m_mov_stat.pos_futura.y = 0;
	m_mov_stat.ulti_sec = 0;
	memset(m_mov_stat.repeticion, 0, sizeof(bool) * SDLMAXKEYS);
	m_mov_stat.vida = vida;
	
}

actor::~actor(void)
{
	if (m_imagen[perfilDer])
		SDL_FreeSurface (m_imagen[perfilDer]);
	if (m_imagen[perfilIzq])
		SDL_FreeSurface (m_imagen[perfilIzq]);
}


inline bool actor::CargarImagenes(const char* RutaImagenPerfilDer, const char* RutaImagenPerfilIzq)
{
	m_imagen[perfilDer] = Imagen::Cargar(RutaImagenPerfilDer);	
	if (m_imagen[perfilDer] == 0)
		return false;

	m_imagen[perfilIzq] = Imagen::Cargar(RutaImagenPerfilIzq);	
	if (m_imagen[perfilIzq] == 0)
		return false;

	return true;
}

inline void actor::SimulaMovimiento(int32_t mov_x, int32_t mov_y, int32_t src_w, int32_t)
{	
	ty_s_conf_mov* mov = mov_conf(make_pair(m_mov_stat.en_curso, m_mov_stat.sentido));
	m_mov_stat.pos_futura.x = (mov_x > 0 && mov_x < src_w - mov->sec_img.at(m_mov_stat.ulti_sec).w) ? static_cast<Sint16>(mov_x) : m_mov_stat.pos_futura.x;
	m_mov_stat.pos_futura.y	= static_cast<Sint16>(mov_y);
}

inline void actor::ConsolidaMovimiento()
{
	m_mov_stat.pos_actual.x = m_mov_stat.pos_futura.x;
	m_mov_stat.pos_actual.y = m_mov_stat.pos_futura.y;
}

inline void actor::InitMovs()
{
	m_mov_stat.repeticion[m_mov_stat.en_curso] = false;
	m_mov_stat.ulti_sec = 0;		
	m_mov_stat.en_curso = descanso;		  
}


inline void actor::DesActivaTipoMovimiento() 
{ 		
  InitMovs();  
}

inline void actor::ActivaTipoMovimiento(int32_t tipo_movimiento, int32_t sentido) 
{		
	m_mov_stat.ulti_sec = 0;
	m_mov_stat.sentido = sentido;
	m_mov_stat.en_curso = tipo_movimiento;		
	
}

}
