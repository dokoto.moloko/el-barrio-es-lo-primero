#include "Musica.hpp"

namespace bp
{

Mix_Music* Musica::Cargar ( const char* RutaMusica )
{
    Mix_Music* musica = 0;
    musica = Mix_LoadMUS(RutaMusica);
    if (musica == 0)
    {
        SDL_SetError(Mix_GetError());
        return 0;
    }

    return musica;
}


} // namespace bp
