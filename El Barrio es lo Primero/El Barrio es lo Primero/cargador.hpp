#ifndef CARGADOR_HPP
#define CARGADOR_HPP

/*--------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------*/
#include "configurador.hpp"

namespace bp
{

/*--------------------------------------------------------------------------------------------------------------
 * CLASS: CARGADOR
 *-------------------------------------------------------------------------------------------------------------*/
class cargador
{
	/*--------------------------------------------------------------------------------------------------------------
	* PROTECTED ATTRIBUTES
	*--------------------------------------------------------------------------------------------------------------*/
	protected:		
		const char*												m_id;
		configurador*											m_configurador;
	
	/*--------------------------------------------------------------------------------------------------------------
	* PROTECTED ATTRIBUTES
	*--------------------------------------------------------------------------------------------------------------*/
	public:					
									cargador					(const char* id) : m_id(id) {}
									cargador					(const char* id, configurador* config) : m_id(id), m_configurador(config) {}
									virtual ~cargador			(void) {}

		virtual const char* 		ObtenerId					() { return m_id; }
		virtual bool 				Ejecutar					() { return true; } 
		virtual bool				iteracion					() { return true; }
		virtual bool				Inicializar					() { return true; } 
		virtual void				Limpiar						() {}  
		virtual	void				GestionaMovs				(Keys_t []) {}
		virtual configurador*&		ObtenerConfigurador			() { return m_configurador; }

};
} // namespace bp
#endif //CARGADOR
