#ifndef EVENTOS_H
#define EVENTOS_H

#include "tipos_glob.hpp"
#include "utils.hpp"

namespace bp
{
class Eventos
{
    public:
        Eventos();
 
        virtual ~Eventos();
 
        virtual void OnEvent(SDL_Event* Event);
 
        virtual void OnInputFocus();
 
        virtual void OnInputBlur();
 
		virtual void OnKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode);
 
        virtual void OnKeyUp(SDLKey sym, SDLMod mod, Uint16 unicode);
 
        virtual void OnMouseFocus();
 
        virtual void OnMouseBlur();
 
        virtual void OnMouseMove(int32_t mX, int32_t mY, int32_t relX, int32_t relY, bool Left,bool Right,bool Middle);
 
        virtual void OnMouseWheel(bool Up, bool Down); 
 
        virtual void OnLButtonDown(int32_t mX, int32_t mY);
 
        virtual void OnLButtonUp(int32_t mX, int32_t mY);
 
        virtual void OnRButtonDown(int32_t mX, int32_t mY);
 
        virtual void OnRButtonUp(int32_t mX, int32_t mY);
 
        virtual void OnMButtonDown(int32_t mX, int32_t mY);
 
        virtual void OnMButtonUp(int32_t mX, int32_t mY);
 
        virtual void OnJoyAxis(Uint8 which, Uint8 axis, Sint16 value);
 
        virtual void OnJoyButtonDown(Uint8 which, Uint8 button);
 
        virtual void OnJoyButtonUp(Uint8 which, Uint8 button);
 
        virtual void OnJoyHat(Uint8 which, Uint8 hat, Uint8 value);
 
        virtual void OnJoyBall(Uint8 which, Uint8 ball, Sint16 xrel, Sint16 yrel);
 
        virtual void OnMinimize();
 
        virtual void OnRestore();
 
        virtual void OnResize(int32_t w,int32_t h);
 
        virtual void OnExpose();
 
        virtual void OnExit();
 
        virtual void OnUser(Uint8 type, int32_t code, void* data1, void* data2);  
		
		virtual void EnableEvent(Uint8 type) { SDL_EventState(type, SDL_ENABLE); }
		virtual void DisableEvent(Uint8 type) {SDL_EventState(type, SDL_IGNORE); }
};
} // namespace bp
#endif // EVENTOS_H
