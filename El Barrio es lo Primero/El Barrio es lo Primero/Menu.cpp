#include "Menu.hpp"

namespace bp
{

using namespace std;

void Menu::NuevoMenu(ty_id id, ty_id idMenuAnt, ty_titulo titulo, string valor, bool estaActivo, bool esDirectorio)
{	
	ty_s_menu nuevoMenu;	
	nuevoMenu.idMenuAnt = idMenuAnt;
	m_root_menu[idMenuAnt].idMenuSig = id;
	nuevoMenu.titulo = titulo;
	nuevoMenu.valor = valor;
	nuevoMenu.esDirectorio = esDirectorio;
	nuevoMenu.estaActivo = estaActivo;
	m_root_menu[id] = nuevoMenu;	

}

bool Menu::IrInicio()
{
	if ( (m_it_nodo = m_root_menu.find(m_root)) != m_root_menu.end() )
		return true;
	else
		return false;
}

ty_m_it_menu Menu::Iterar()
{
	static unsigned submenu = 0;
	ty_id l_it_clave = make_pair((*m_it_nodo).first.first, submenu++);
	ty_m_it_menu l_it_nodo = m_root_menu.find(l_it_clave);
	if (l_it_nodo == m_root_menu.end())	
		submenu = 0;

	return l_it_nodo;
}

bool Menu::AvanArriba()
{
	ty_id l_it_clave = make_pair((*m_it_nodo).first.first, (*m_it_nodo).first.second-1);
	ty_m_it_menu l_it_nodo = m_root_menu.find(l_it_clave);
	if (l_it_nodo != m_root_menu.end())
	{
		m_it_nodo = l_it_nodo;
		return true;
	}
	else
		return false;	
}

bool Menu::AvanAbajo()
{

	ty_id l_it_clave = make_pair((*m_it_nodo).first.first, (*m_it_nodo).first.second+1);	
	ty_m_it_menu l_it_nodo = m_root_menu.find(l_it_clave);
	if (l_it_nodo != m_root_menu.end())
	{
		m_it_nodo = l_it_nodo;
		return true;
	}
	else
		return false;	
}

bool Menu::AvanDer()
{
	ty_m_it_menu l_it_nodo = m_root_menu.find(make_pair((*m_it_nodo).second.idMenuSig.first, 0));
	if (l_it_nodo != m_root_menu.end())
	{		
		m_it_nodo = l_it_nodo;		
		return true;
	}
	else
		return false;
}

bool Menu::AvanIzq()
{
	ty_m_it_menu l_it_nodo = m_root_menu.find((*m_it_nodo).second.idMenuAnt);
	if (l_it_nodo != m_root_menu.end())
	{
		m_it_nodo = l_it_nodo;
		return true;
	}
	else
		return false;
}

}  // namespace bp
