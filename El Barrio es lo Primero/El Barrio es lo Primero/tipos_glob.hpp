#ifndef TIPOS_GLOB_HPP
#define TIPOS_GLOB_HPP

/*--------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------*/
#ifdef _MSC_VER
    typedef SSIZE_T ssize_t;
    typedef __int16 int16_t;
    typedef unsigned __int16 uint16_t;
    typedef __int32 int32_t;
    typedef unsigned __int32 uint32_t;
    typedef __int64 int64_t;
    typedef unsigned __int64 uint64_t;
#else
    #include <stdint.h>
#endif
#ifdef APPLE
	#include "/opt/local/include/SDL/SDL.h"
	#include "/opt/local/include/SDL/SDL_image.h"
	#include "/opt/local/include/SDL/SDL_mixer.h"
	#include "/opt/local/include/SDL/SDL_ttf.h"
#endif

#ifdef LINUX
	#include <SDL/SDL.h>
	#include <SDL/SDL_image.h>
	#include <SDL/SDL_mixer.h>
	#include <SDL/SDL_ttf.h>
#endif

#if defined APPLE | defined LINUX
	#include <sys/types.h>
	#include <dirent.h>
	#include <fcntl.h>	
	#include <unistd.h>
    #define GetCurrentDir getcwd	
#endif

#ifdef WIN
	#define _CRT_SECURE_NO_WARNINGS
	#include "dirent.h"
	#include <direct.h>
	#include "SDL.h"
	#include "SDL_image.h"
	#include "SDL_mixer.h"
	#include "SDL_ttf.h"
	#define GetCurrentDir _getcwd	
	#define mkdir _mkdir
#endif

#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <sys/stat.h>

#if defined LINUX | defined APPLE
    #define mkdir(path) mkdir(path, 0777)
#endif

#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <utility>


/*--------------------------------------------------------------------------------------------------------------
 * MACROS
 *-------------------------------------------------------------------------------------------------------------*/
#define SDLMAXKEYS 500

namespace bp
{

/*--------------------------------------------------------------------------------------------------------------
 * GLOBAL TYPES 
 *-------------------------------------------------------------------------------------------------------------*/
typedef unsigned short									UShort_t;	// UNSIGNED SHORT
typedef short											Short_t;	// SIGNED SHORT
typedef const short										CoShort_t;	// SIGNED SHORT
typedef std::string										Str_t;  	// SIMPLE CL.STRING	
typedef const std::string								CoStr_t;	// CONST CL.STRING

typedef char*		                                    CStr_t;		// SIMPLE ANSI C STRING	
typedef const char*		                                CoCStr_t;	// CONST ANSI C STRING

typedef char											Char_t;		// SIMPLE CHAR		
typedef const char		                                CoChar_t;	// CONST CHAR 

typedef const int32_t									CoUInt_t;	// CONST UNSIGNED INT

typedef const int32_t										CoInt_t;	// CONST SIGNED INT


typedef std::stringstream								Buffer_t;	// STRING-STREAM

typedef DIR*											Dir_t;		
typedef struct dirent*									DirDetail_t;

typedef std::vector<std::string>						VStr_t;		// VECTOR < CL.STRING >
typedef std::map<std::string,std::string>				MStr_t;

typedef	struct											
{
	uint32_t									type;	// {0: Fichero , 1: Directorio, 2: Otros}
	Str_t									name;
}														File_t;
typedef std::vector<File_t>								VFile_t;	// VECTOR < File_t >


typedef struct											
{
			bool							Press;	
			bool							UnPress;
}														Keys_t;
typedef struct											
{
			Uint8							R; 
			const Uint8						G; 
			const Uint8						B;
}														Color_t;


/*--------------------------------------------------------------------------------------------------------------
* ENUMS
*--------------------------------------------------------------------------------------------------------------*/			
enum						Sentido						{ perfilDer, perfilIzq, Sentido_LEN };		
enum						TipoMov						{	golpeo_mano, patada, patada_circular,	
															patada_baja, golpeo_mano_bajo, golpe_mano_alto,
															bola_fuego, salto_largo, patada_helicop, 
															avance, retorno, descanso, agacharse,
															salto_arriba, recep_golpe_N1, recep_golpe_N2,
                                                            recep_golpe_N3,
															TipoMov_LEN };

const char				   TipoMovToString[][30] =		{"golpeo_mano", "patada", "patada_circular",	
															"patada_baja", "golpeo_mano_bajo", "golpe_mano_alto",
															"bola_fuego", "salto_largo", "patada_helicop", 
															"avance", "retorno", "descanso", "agacharse",
															"salto_arriba", "recep_golpe_N1", "recep_golpe_N2",
                                                            "recep_golpe_N3",
															"TipoMov_LEN" };



} // namespace bp

#endif //TIPOS_GLOB_HPP
