#ifndef FX_HPP
#define FX_HPP

#include "utils.hpp"

namespace bp
{

class FX
{
  private :
    Mix_Chunk* 			m_fx;

    
    
  public:
    FX(void);
    ~FX(void);

    bool			Cargar				(const char* RutaFx);
    void			Sonar				();
    void			Parar				();
};

}

#endif // FX_HPP
