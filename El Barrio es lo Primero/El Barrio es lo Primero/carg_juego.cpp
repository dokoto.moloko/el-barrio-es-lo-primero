#include "carg_juego.hpp"

namespace bp
{

using namespace std;

Carg_Juego::Carg_Juego(const char* id) :
        
	cargador(id, new configurador),
        m_continuar(true)

{
    m_configurador->Canal_audio() = 2;
    m_configurador->Caudal_audio() = 4096;
    m_configurador->Formato_audio() = AUDIO_S16;
    m_configurador->Frecuencia_audio() = 22050;    
	m_configurador->Ruta_icono_juego() = Paths.ObtieneRuta("ICONO_TITULO");
    m_configurador->Titulo_juego() = "El barrio, es lo primero !";
    m_configurador->Pantalla_Ancho() = 800;
    m_configurador->Pantalla_Alto() = 600;
    m_configurador->Pantalla_Profundidad() = 32;
    m_configurador->Imagenes_por_Segundo() = 60;
}

Carg_Juego::~Carg_Juego()
{
    Limpiar();
}

bool Carg_Juego::Ejecutar()
{
    if ( (Inicializar()) == false) return false;    
    if ( (iteracion()) == false) return false;

	return true;
}

bool Carg_Juego::iteracion()
{
	m_elementos[m_configurador->FASE_INTRO] = new Fase_Intro(m_configurador->FASE_INTRO, m_configurador);
    if ( (m_elementos[m_configurador->FASE_INTRO]->Ejecutar()) == false ) return false;    
    while (m_continuar)
    {
		m_elementos[m_configurador->FASE_CONFIG] = new Fase_Config(m_configurador->FASE_CONFIG, m_configurador);
        if (!(m_continuar = m_elementos[m_configurador->FASE_CONFIG]->Ejecutar()))
		{
			delete m_elementos[m_configurador->FASE_CONFIG];
			m_elementos[m_configurador->FASE_CONFIG] = 0;
			break;
		}
		
		// TESTING FASE 01
		if (m_configurador->ObtenerActor() != 0)
		{
			m_elementos[m_configurador->FASE_DEBUG] = new Fase_DBG(m_configurador->FASE_DEBUG, m_configurador);
			m_elementos[m_configurador->FASE_DEBUG]->Ejecutar();
			delete m_elementos[m_configurador->FASE_DEBUG];
			m_elementos[m_configurador->FASE_DEBUG] = 0;
		}

    }
	delete m_elementos[m_configurador->FASE_INTRO];	
	m_elementos[m_configurador->FASE_INTRO] = 0;

	return true;
}

bool Carg_Juego::InicializarSistemas()
{
    // SISTEMAS : VIDIEO Y AUDIO
    if ( SDL_Init ( SDL_INIT_VIDEO | SDL_INIT_AUDIO ) < 0 )
    {
        SDL_SetError(SDL_GetError()); 
        return false;
    }

    // CONFG SISTEMA : VIDEO
    if ( ( m_configurador->Escenario() = SDL_SetVideoMode ( m_configurador->Pantalla_Ancho(), m_configurador->Pantalla_Alto(), m_configurador->Pantalla_Profundidad(), SDL_HWSURFACE | SDL_DOUBLEBUF ) ) == 0 )
    {
        SDL_SetError(SDL_GetError()); 
        return false;
    }

    // CONFG SISTEMA : AUDIO
    if ( Mix_OpenAudio ( m_configurador->Frecuencia_audio(), m_configurador->Formato_audio(), m_configurador->Canal_audio(), m_configurador->Caudal_audio() ) )
    {
        SDL_SetError(SDL_GetError()); 
        return false;
    }
 
    // CONFG SISTEMA: LETRAS
    if (TTF_Init() < 0)
    {
        SDL_SetError(SDL_GetError()); 
        return false;
    }

	return true;
}

bool Carg_Juego::InicializarVentana()
{
    if ( (m_configurador->IconoTitulo() = Imagen::Cargar(m_configurador->Ruta_icono_juego().c_str())) == 0)    
		return false;
    
    SDL_WM_SetCaption ( m_configurador->Titulo_juego().c_str(), m_configurador->Titulo_juego().c_str() );
    SDL_WM_SetIcon ( m_configurador->IconoTitulo(), NULL );

    return true;
}


bool Carg_Juego::InicializarFases()
{
	return true;
}


bool Carg_Juego::Inicializar()
{
    if ( (InicializarSistemas()) == false) return false;    
    if ((InicializarVentana()) == false) return false;    
    if ((InicializarFases()) == false) return false;

	return true;
}

void Carg_Juego::Limpiar()
{
    for (map<const char*, cargador*>::iterator mit = m_elementos.begin(); mit != m_elementos.end(); mit++)
	{
		if ((*mit).second != 0)
			delete (*mit).second;
	}
    
    if (m_configurador->Escenario())
        SDL_FreeSurface(m_configurador->Escenario());

	if (m_configurador->IconoTitulo())
		SDL_FreeSurface(m_configurador->IconoTitulo());

	if (m_configurador)
		delete m_configurador;
	m_configurador = 0;

    Mix_CloseAudio();
    TTF_Quit();
    SDL_Quit();
}

}
