#include "Fase_Intro.hpp"

namespace bp
{

using namespace std;

Fase_Intro::Fase_Intro (const char* id, configurador* config) :

        fase(id, config),
        m_ejecutandose ( true ),
        m_ttf_titulo(),
        m_ruta_ttf ( Paths.ObtieneRuta("FUENTES_FASE_INTRO") ),        
        m_musica_intro(0),
        m_capa_historia(0)

{
}

bool Fase_Intro::CargarHistoria()
{
    const char* RutaTexto =  Paths.ObtieneRuta("TEXTO_FASE_INTRO");
    fstream in;
    in.open(RutaTexto);
    if (in.is_open())
    {
        string line;
        while (in.good())
        {
            getline(in, line);
            m_historia.push_back(line);
        }
        in.close();
        return true;
    }
    else
    {
        SDL_SetError("El fichero de texto para la intruduccion no existe en la ruta : %s\n", RutaTexto );
        return false;
    }

}


Fase_Intro::~Fase_Intro ( void )
{
    Limpiar();
}

bool Fase_Intro::Inicializar()
{
    if ( (CargarHistoria()) == false) return false;
    SDL_initFramerate ( &m_manejadorImgPorSeg );
    SDL_setFramerate ( &m_manejadorImgPorSeg, m_configurador->Imagenes_por_Segundo() / 2 );

    if ( (m_musica_intro = Musica::Cargar(Paths.ObtieneRuta("MUSICA_FASE_INTRO"))) == 0) return false;
    Musica::Sonar(m_musica_intro);

    return true;
}

bool Fase_Intro::Ejecutar()
{
    if ((Inicializar()) == false) return false;
    if ((Iteracion()) == false) return false;
    Musica::Parar();

    return true;
}

void Fase_Intro::OnEvent ( SDL_Event* evento )
{
    Eventos::OnEvent ( evento );
}

void Fase_Intro::Salir()
{
    m_ejecutandose = false;
}

bool Fase_Intro::Iteracion()
{
    if ((CreaIntro()) == false) return false;
    while ( m_ejecutandose )
    {
        while ( SDL_PollEvent ( &m_evento ) )
        {
            OnEvent ( &m_evento );
        }
        if ((Dibujar()) == false) return false;
        SDL_framerateDelay ( &m_manejadorImgPorSeg );
    }

    return true;
}

void Fase_Intro::Limpiar()
{
    if (m_capa_historia)
        SDL_FreeSurface ( m_capa_historia );
    
    if (m_musica_intro)
        Mix_FreeMusic(m_musica_intro);

    m_capa_historia = 0;
    m_musica_intro = 0;
}

bool Fase_Intro::Dibujar()
{
    Imagen::LimpiarImagen(m_configurador->Escenario());
    if ((DibujaIntro()) == false) return false;
    SDL_Flip ( m_configurador->Escenario() );

    return true;
}

bool Fase_Intro::DibujaIntro()
{
    int32_t x = 0;
    static int32_t y = m_configurador->Pantalla_Alto();

    if ( (Imagen::Dibujar (m_configurador->Escenario(), m_capa_historia, x, y, 0, 0, m_capa_historia->w, m_capa_historia->h )) == false)
        return false;
    if (y > 10)
        y--;

    return true;
}


bool Fase_Intro::CreaIntro()
{
    if ( (m_capa_historia = Imagen::CrearImagen(0, m_configurador->Pantalla_Ancho(), m_configurador->Pantalla_Alto(), m_configurador->Escenario())) == false)
        return false;

    Imagen::LimpiarImagen(m_capa_historia);
    int32_t alto = 20, x = 0, y = 0;
    for (vector<string>::iterator it = m_historia.begin(); it != m_historia.end(); it++)
    {
        if ((*it).size() != 0)
        {
            if ( (GeneraTTF(m_capa_historia, (*it).c_str(), x, y, alto )) == false)
                return false;
        }
        y += 22;
    }

    return true;
}

bool Fase_Intro::GeneraTTF (SDL_Surface* ImagenDestino, const char* parrafo, int32_t x, int32_t y, int32_t alto )
{
    // Se carga la fuente
    Texto* ttf_parrafo = new Texto;
    if ( (ttf_parrafo->CargaFuente ( m_ruta_ttf, alto )) == false)
        return false;

    // Se configura las letras
    SDL_Color colorTitulo = {5, 10, 10, 0};
    SDL_Color colorFondoTitulo = {255, 255, 255, 0};
    ttf_parrafo->PonerColor ( colorTitulo );
    ttf_parrafo->PonerColorFondo ( colorFondoTitulo );

    // Se genera un capa con las letras
    SDL_Surface* img_titulo = 0;
    if ( (img_titulo = ttf_parrafo->Dibujar (parrafo)) == 0)
    {
        if (ttf_parrafo) delete ttf_parrafo;
        if (img_titulo) SDL_FreeSurface(img_titulo);
        return false;
    }

    x = (ImagenDestino->w - img_titulo->w) / 2;
    if ( (Imagen::Dibujar (ImagenDestino, img_titulo, x, y, 0, 0, img_titulo->w, img_titulo->h )) == false)
    {
        if (img_titulo) SDL_FreeSurface(img_titulo);
        if (ttf_parrafo) delete ttf_parrafo;
        return false;
    }

    // Se limpia lo usado
    if (img_titulo) SDL_FreeSurface(img_titulo);
    if (ttf_parrafo) delete ttf_parrafo;

    return true;
}


void Fase_Intro::OnKeyDown ( SDLKey , SDLMod , Uint16 )
{
    Salir();
}

} // namespace bp
