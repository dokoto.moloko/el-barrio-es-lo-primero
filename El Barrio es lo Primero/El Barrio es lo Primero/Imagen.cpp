#include "Imagen.hpp"

namespace bp
{

SDL_Surface* Imagen::CrearImagen(Uint32 opciones, int32_t ancho, int32_t alto, const SDL_Surface* Imagen)
{
	SDL_Surface* ImagenDestino = 0;
    const SDL_PixelFormat& fmt = *(Imagen->format);
    ImagenDestino = SDL_CreateRGBSurface(opciones, ancho, alto, fmt.BitsPerPixel, fmt.Rmask,fmt.Gmask,fmt.Bmask,fmt.Amask );
    if (ImagenDestino == 0)
		return 0;     
	return ImagenDestino;
}


SDL_Surface* Imagen::Cargar(const char* RutaFichero)
{
	SDL_Surface* ImagenDestino = 0;
    SDL_Surface* ImagenTemporal = 0;

    if ((ImagenTemporal = IMG_Load(RutaFichero)) == 0)
        return 0;
    
    ImagenDestino = SDL_DisplayFormat(ImagenTemporal);
    SDL_FreeSurface(ImagenTemporal);

    return ImagenDestino;
}

void Imagen::CrearTransparencia(SDL_Surface*& ImagenDestino, const Uint8 R, const Uint8 G, const Uint8 B)
{	
    SDL_SetColorKey(ImagenDestino, SDL_SRCCOLORKEY | SDL_RLEACCEL, SDL_MapRGB(ImagenDestino->format, R, G, B));
}


bool Imagen::Dibujar(SDL_Surface* ImagenDestino, SDL_Surface* ImagenOrigen, int32_t x, int32_t y)
{
	SDL_Rect Delimitacion = {0, 0, 0, 0};

    Delimitacion.x = static_cast<Sint16>(x);
    Delimitacion.y = static_cast<Sint16>(y);

    SDL_BlitSurface(ImagenOrigen, NULL, ImagenDestino, &Delimitacion);
	if (ImagenDestino == 0)
            return false;

    return true;
}

/**************************************************************************
* PosicionX, PosicionY = Posicion en la pantalla
* PosicionRecorteX, PosicionRecorteY = Posicion de corte, es decir, desde donde se recorta la imagen
* Ancho, Alto = Ancho y alto que se desea cortar
***************************************************************************/
bool Imagen::Dibujar(SDL_Surface* ImagenDestino, SDL_Surface* ImagenOrigen, int32_t PosicionX, int32_t PosicionY, int32_t PosicionRecorteX, int32_t PosicionRecorteY, int ancho, int alto)
    {
        return Dibujar(ImagenDestino, ImagenOrigen, PosicionX, PosicionY, PosicionRecorteX, PosicionRecorteY,
                static_cast<uint16_t>(ancho), static_cast<uint16_t>(alto));
                
    }
    
bool Imagen::Dibujar(SDL_Surface* ImagenDestino, SDL_Surface* ImagenOrigen, int32_t PosicionX, int32_t PosicionY, int32_t PosicionRecorteX, int32_t PosicionRecorteY, uint16_t ancho, uint16_t alto)
{
    SDL_Rect DelimitacionImgDestino = {0, 0, 0, 0};

    DelimitacionImgDestino.x = static_cast<Sint16>(PosicionX);
    DelimitacionImgDestino.y = static_cast<Sint16>(PosicionY);

    SDL_Rect DelimitacionImgOrigen = {0, 0, 0, 0};

    DelimitacionImgOrigen.x = static_cast<Sint16>(PosicionRecorteX);
    DelimitacionImgOrigen.y = static_cast<Sint16>(PosicionRecorteY);
    DelimitacionImgOrigen.w = static_cast<Uint16>(ancho);
    DelimitacionImgOrigen.h = static_cast<Uint16>(alto);

    SDL_BlitSurface(ImagenOrigen, &DelimitacionImgOrigen, ImagenDestino, &DelimitacionImgDestino);
	if (ImagenDestino == 0)
            return false;

    return true;
}
    
bool Imagen::DibujarRectangulo(SDL_Surface* ImagenDestino, int32_t x, int32_t y, int ancho, int alto, Uint32 color)
    {
        return DibujarRectangulo(ImagenDestino, x, y, static_cast<uint16_t>(ancho), static_cast<uint16_t>(alto), color);
    }
    
bool Imagen::DibujarRectangulo(SDL_Surface* ImagenDestino, int32_t x, int32_t y, uint16_t ancho, uint16_t alto, Uint32 color)
{
  if ( alto == 0 || ancho == 0 ) return false;  
  SDL_Rect DelimitacionRectangulo = {static_cast<Sint16>(x), static_cast<Sint16>(y), static_cast<Uint16>(ancho), static_cast<Uint16>(alto)};
  SDL_FillRect(ImagenDestino, &DelimitacionRectangulo, color);
  
  return true;
}

void Imagen::LimpiarImagen(SDL_Surface* ImgenDestino)
{
    SDL_Rect delimitadorImagen;
    delimitadorImagen.x = delimitadorImagen.y = 0;
    delimitadorImagen.w = static_cast<Uint16> ( ImgenDestino->w );
    delimitadorImagen.h = static_cast<Uint16> ( ImgenDestino->h );
    Uint32 colorFondo = SDL_MapRGB ( ImgenDestino->format, 255, 255, 255 );
    SDL_FillRect ( ImgenDestino, &delimitadorImagen, colorFondo );
}


} // namespace bp
